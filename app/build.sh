## This script bundles angular application ##
## Edit the build variables or pass their from cli ##

## API_URL corresponds to the address where the backend is running ##
## TURN_USER corresponds to a valid user name stored in CoTurn database ##
## TURN_PASS corresponds to the password of the previous user ##

## Example ##
## API_URL=domain.com TURN_USER=user TURN_PASS=pass npm run build ##

#BUILD VARs
API_URL=$API_URL
TURN_USER=$TURN_USER
TURN_PASS=$TURN_PASS

#clean
rm -rf www/*
#bundle, minify, mangle
NODE_ENV=production node node_modules/.bin/webpack 
#copy resources
cp -r src/resources www/
