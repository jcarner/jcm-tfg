import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngSanitize from 'angular-sanitize';
import ngMaterial from 'angular-material';
import ngAria from 'angular-aria';
import ngAnimate from 'angular-animate';
import 'angular-material/angular-material.min.css';
import hmTouchEvents from 'angular-hammer';
import ngTouch from 'angular-touch';
import ngclipboard from 'ngclipboard';

import {appComponent} from './app.component';

import {home} from './components/home';
import {common} from './components/common';
import {rooms} from './components/rooms';
import {addRoom} from './components/addRoom';
import {room} from './components/room';
import {profile} from './components/profile';

import {style} from './shared/style';
import {thirdParty} from './shared/3rd-party'


const config = ($stateProvider, $urlRouterProvider) => {
    'use strict';
    $stateProvider
        .state('app',{
            abstract: true,
            template: '<app></app>'
        });
    $urlRouterProvider.otherwise('/');
};

config.$inject = ['$stateProvider','$urlRouterProvider'];

const app = angular.module('app', [
    uiRouter,
    ngSanitize,
    ngMaterial,
    ngAria,
    ngAnimate,
    hmTouchEvents,
    ngTouch,
    ngclipboard,

    home.name,
    common.name,
    rooms.name,
    addRoom.name,
    room.name,
    profile.name,

    style.name,
    thirdParty.name
])
    .config(config)
    .component('app', appComponent);

export {app};