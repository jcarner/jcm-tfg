import angular from 'angular';

const config = ($mdThemingProvider, $mdIconProvider) => {

    $mdIconProvider
        .icon('menu','../resources/icons/menu.svg')
        .icon('mic','../resources/icons/mic.svg')
        .icon('copy','../resources/icons/ios-copy.svg')
        .icon('more','../resources/icons/more.svg')
        .icon('facebook','../resources/icons/facebook.svg')
        .icon('google','../resources/icons/googleplus.svg')
        .icon('close','../resources/icons/close.svg')
        .icon('download','../resources/icons/download.svg')
        .icon('cam-out','../resources/icons/cam-out.svg')
        .icon('reload','../resources/icons/reload.svg')
        .icon('plus','../resources/icons/plus.svg')
        .icon('true', '../resources/icons/true.svg')
        .icon('false', '../resources/icons/false.svg')
        .icon('play', '../resources/icons/play.svg')
        .icon('pause', '../resources/icons/pause.svg')
        .icon('next', '../resources/icons/next.svg')
        .icon('previous', '../resources/icons/ios-skipbackward-outline.svg')
        .icon('volume-high', '../resources/icons/ios-volume-high.svg')
        .icon('mute', '../resources/icons/ios-volume-low.svg')
        .icon('chat', '../resources/icons/chat.svg')
        .icon('user', '../resources/icons/user.svg')
        .icon('profile', '../resources/icons/profile.svg')
        .icon('back', '../resources/icons/back.svg')
        .icon('minimize', '../resources/icons/ios-minus-empty.svg')
        .icon('up', '../resources/icons/ios-arrow-up.svg')
        .icon('down', '../resources/icons/ios-arrow-down.svg')
        .icon('forward', '../resources/icons/forward.svg')
        .icon('folder', '../resources/icons/folder.svg')
        .icon('file', '../resources/icons/file.svg')
        .icon('history', '../resources/icons/history-clock-button.svg')
        .icon('image', '../resources/icons/image.svg');
    $mdIconProvider
        .iconSet('ionicon', '../resources/icons/ionicons.svg');

    $mdThemingProvider.theme('default')
        .primaryPalette('light-blue')
        .accentPalette('blue')
        .warnPalette('amber')
        .dark();

};

config.$inject = ['$mdThemingProvider', '$mdIconProvider'];

export const style = angular.module('style', [])
    .config(config);