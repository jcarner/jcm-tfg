import angular from 'angular';

import {FacebookService} from './facebook.service';
import {GoogleService} from './google.service';

const run = ($window) => {
    // $window.fbAsyncInit = () => {
    //     FB.init({
    //         appId: '326890037706883',
    //         status: true,
    //         cookie: true,
    //         xfbml: true
    //     });
    // };
    //
    // ((d) => {
    //     let js,
    //         id = 'facebook-jssdk',
    //         ref = d.getElementsByTagName('script')[0];
    //
    //         if (d.getElementById(id)) {
    //             return;
    //         }
    //
    //         js = d.createElement('script');
    //         js.id = id;
    //         js.async = true;
    //         js.src = '//connect.facebook.net/en_US/all.js';
    //
    //         ref.parentNode.insertBefore(js, ref);
    // })(document);

    // (()=> {
    //     let js = document.createElement('script');
    //     js.async = true;
    //     js.onload = () => {
    //         gapi.load('client:auth2', () => {
    //             gapi.client.init({
    //                 'apikey': 'AIzaSyCyaMMry5nqXlOorYYZGOg6CONTNwpLt74'
    //             });
    //         });
    //
    //     };
    //     js.src = 'https://apis.google.com/js/api.js';
    //     document.getElementsByTagName('script')[0].parentNode.appendChild(js);
    // })()
};

run.$inject = ['$window'];

export const social = angular.module('social', [])
    .run(run)
    .factory('FacebookService', FacebookService)
    .factory('GoogleService', GoogleService);