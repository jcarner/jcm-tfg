const FacebookService = ($window, $q) => {

    const loaded = false;

    const loadAPI = () => {
        let defer = $q.defer();
        $window.fbAsyncInit = () => {
            $window.FB.init({
                appId: `${__FB_APP_ID__}`,
                status: true,
                cookie: true,
                xfbml: true
            });
            defer.resolve();
        };

        ((d) => {
            let js,
                id = 'facebook-jssdk',
                ref = d.getElementsByTagName('script')[0];

            if (d.getElementById(id)) {
                defer.resolve();
            }

            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = '//connect.facebook.net/en_US/all.js';

            ref.parentNode.insertBefore(js, ref);
        })(document);

        return defer.promise;
    };

    const getMe = () => {
        let defer = $q.defer();
        try {
            FB.getLoginStatus((res) => {
                if (res.status === 'connected') {
                    FB.api('/me', (response) => {
                        if (!response || response.error) {
                            defer.reject();
                        } else {
                            defer.resolve(response);
                        }
                    }, {fields: 'birthday,email,gender,hometown,name,picture'});
                } else {
                    FB.login((res) => {
                        FB.api('/me', (res) => {
                            if (!res || res.error) {
                                defer.reject();
                            } else {
                                defer.resolve(res);
                            }
                        }, {fields: 'birthday,email,gender,hometown,name,picture'});
                    });
                }
            });
        } catch(err) {
            loadAPI()
                .then(() => {
                    FB.getLoginStatus((res) => {
                        if (res.status === 'connected') {
                            FB.api('/me', (response) => {
                                if (!response || response.error) {
                                    defer.reject();
                                } else {
                                    defer.resolve(response);
                                }
                            }, {fields: 'birthday,email,gender,hometown,name,picture'});
                        } else {
                            FB.login((res) => {
                                FB.api('/me', (res) => {
                                    if (!res || res.error) {
                                        defer.reject();
                                    } else {
                                        defer.resolve(res);
                                    }
                                }, {fields: 'birthday,email,gender,hometown,name,picture'});
                            });
                        }
                    });
                })
        }

        return defer.promise;
    };

    const getLastName = () => {
        let defer = $q.defer();
        FB.api('/me', {
            fields: 'last_name'
        }, (response) => {
            if (!response || response.error) {
                defer.reject();
            } else {
                defer.resolve(response);
            }
        });
        return defer.promise;
    };

    return {
        getMe,
        getLastName
    };
};

FacebookService.$inject = ['$window', '$q'];

export {FacebookService};