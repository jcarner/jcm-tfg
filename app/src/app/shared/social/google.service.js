const GoogleService = ($q) => {

    let googleAuth = null;

    const loadAPI = (callback) => {
        let defer = $q.defer();
        let api = document.createElement('script');
        api.async = false;
        api.src = 'https://apis.google.com/js/api.js';
        api.onload = callback ? callback : () => {
                gapi.load('client:auth2', () => {
                    gapi.auth2.init({
                        apiKey: `${__GAPI_KEY__}`,
                        clientId: `${__GAPP_ID__}`,
                        scope: 'profile email'
                    }).then((res) => {
                        googleAuth = gapi.auth2.getAuthInstance();
                        googleAuth.then(
                            () => {defer.resolve();},
                            () => {}
                        );
                        googleAuth.signIn();
                    });
                });
            };
        document.getElementsByTagName('body')[0].appendChild(api);
        return defer.promise;
    };


    const getMe = () => {
        let defer = $q.defer();
        if(!googleAuth) {
            loadAPI()
                .then(() => {
                    let user = googleAuth.currentUser.get().w3;
                    defer.resolve({name: user.ig,email: user.U3,photo: user.Paa});
                });
        } else  {
            let user = googleAuth.currentUser.get().w3;
            defer.resolve({name: user.ig,email: user.U3,photo: user.Paa});
        }
        return defer.promise;
    };

    return {
        getMe
    };
};

GoogleService.$inject = ['$q'];

export {GoogleService};