const PeerConnectionService = () => {

    const pc_config = {
        'iceServers': [
            {url: 'stun:23.21.150.121'},
            {url: 'stun:stun.1.google.com:19302'}
        ]
    };

    const pc_constrains = {
        'optional': [{RtpDataChannels: true}]
    };

    const data_constraint = {reliable: false};

    let pc = null,
        recvDChannel = null,
        sendDChannel = null;

    const createPeerConnection = () => {
        try {
            pc = new RTCPeerConnection(pc_config, pc_constrains);
            pc.onicecandidate = (event) => {
                if (event.candidate) {
                    sendMessage({
                        type: candidate,
                        label: event.candidate.sdpMLineIndex,
                        id: event.candidate.sdpMid,
                        candidate: event.candidate.candidate
                    });
                }
            };
            pc.ondatachannel = (event) => {
                console.log('Receive Channel Callback');
                recvDChannel = event.channel;
                recvDChannel.onmessage = (event) => {
                    try {
                        let msg = JSON.parse(event.data);
                        if (msg.type === 'file') {
                            //todo
                            onFileReceived(msg.name, msg.size, msg.data);
                        }
                    } catch (e) {
                        console.log(e.message);
                    }

                };
                recvDChannel.onopen = () => {
                    if (recvDChannel.readyState === 'open') {
                        sendDChannel = recvDChannel;
                    }
                };
                recvDChannel.onclose = () => {
                    console.log('Receive Channel Closed');
                };
            }
        } catch (e) {
            console.log(e);
            return;
        }
    };

    const failureCallback = (e) => {
        console.log('failure callback ' + e.message);
    };

    const createDataChannel = (role) => {
        try {
            //todo name of dtachannel
            sendDChannel = pc.createDataChannel('', data_constraint);
            sendDChannel.onopen = () => {
                if (sendDChannel.readyState === 'open') {
                    sendDChannel.onmessage = () => {
                        try {
                            let msg = JSON.parse(event.data);
                            if (msg.type === 'file') {
                                //todo
                                onFileReceived(msg.name, msg.size, msg.data);
                            }
                        } catch (e) {
                            console.log(e.message);
                        }
                    };
                }
            };
            sendDChannel.onclose = () => {
                console.log('Send Channel closed');
            };
        }
    };

    const sendFile = (file) => {
        sendDChannel.send(file);
    };

    return {
        createPeerConnection,
        sendFile
    }
};