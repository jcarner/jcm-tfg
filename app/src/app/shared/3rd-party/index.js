import angular from 'angular';
import JSZip from 'jszip';

export const thirdParty = angular.module('3rdParty', [])
    .constant('JSZip', JSZip);

