
import {appController} from './app.controller';
import template from './app.html';

export const appComponent = {
    controller: appController,
    template
};
