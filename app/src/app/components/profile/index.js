import angular from 'angular';

import {profileComponent} from './profile.component';

const config = ($stateProvider) => {
    $stateProvider
        .state('app.profile', {
            url: '^/profile/:user',
            component: 'profile',
            resolve: {
                user: ['$stateParams', ($stateParams) => {
                    return {
                        name: 'Josep'
                    }
                }]
            }
        })
};

config.$inject = ['$stateProvider'];

export const profile = angular.module('profile', [])
    .config(config)
    .component('profile', profileComponent);