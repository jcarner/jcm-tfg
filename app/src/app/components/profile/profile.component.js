import template from './profile.html';
import './profile.scss';
import {profileController} from './profile.controller';

export const profileComponent = {
    template,
    controller: profileController,
    bindings: {
        user: '<'
    }
};