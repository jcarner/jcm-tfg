import angular from 'angular';

import {addRoomComponent} from './addRoom.component';

const config = ($stateProvider) => {
    $stateProvider
        .state('app.addRoom', {
            url: '^/addRoom',
            component: 'addRoom'
        })
};

config.$inject = ['$stateProvider'];

export const addRoom = angular.module('addRoom', [])
    .config(config)
    .component('addRoom', addRoomComponent);