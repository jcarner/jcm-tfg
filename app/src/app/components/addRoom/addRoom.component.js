import template from './addRoom.html';
import './addRoom.scss';
import {addRoomController} from './addRoom.controller';

export const addRoomComponent = {
    template,
    controller: addRoomController
}