'use strict';
class chatController {
    constructor(RoomSevice){
        this.RoomService = RoomSevice;

        this.chatInput = {};

        this.$onInit = () => {
            this.id = Math.random().toString(36).substring(7);
            if(!this.remoteUser) {
                this.chatInput.placeholder = 'Type a message';
            } else {
                this.chatInput.placeholder = `Type a message to ${this.remoteUser.name? this.remoteUser.name : this.remoteUser.streamId}`;
            }
        };

        this.$onChanges = (changes) => {
            if(changes.content && !changes.content.isFirstChange()) {
                this.scrollBottom();
            }
        };

        this.$doCheck = () => {}
    }

    send() {
        if (!this.msg || this.msg === '') {
            return;
        }
        let msg = {
            content: this.msg,
            fromName: this.localUser.name,
            fromId: this.localUser.connectionId,
            photo: this.localUser.photo ? this.localUser.photo : null,
            type: 'start'
        };
        if(this.remoteUser) { msg.to = this.remoteUser.connectionId; }
        this.content.push(msg);
        //reset the input container
        this.chatInput.height = 'unset';
        this.msg = '';

        this.RoomService.sendMessage(msg);
        this.scrollBottom();
        //this.onNewMsg({id: this.user.connectionId});
    }

    checkMsg(event) {
        if (event.keyCode == 13) {
            if (this.msg == '') {
                event.preventDefault();
            } else if(event.shiftKey || event.ctrlKey) {
                event.preventDefault();
                this.send();
            } else {
                let chatInput = event.currentTarget;
                if (chatInput.scrollHeight > chatInput.clientHeight) {
                    //stretch the input container
                    this.chatInput.height = `${chatInput.scrollHeight + 15}px`;
                }
            }
        }
    }

    scrollBottom() {
        let chat = document.getElementById(this.id);
        chat.scrollTop += chat.scrollHeight;
    }
}

chatController.$inject = ['RoomService'];

export {chatController};