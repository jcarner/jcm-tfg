import template from './chat.html'
import './chat.scss';
import {chatController} from './chat.controller';

export const chatComponent = {
    template,
    controller: chatController,
    bindings: {
        content: '<',
        localUser: '<',
        remoteUser: '<'
    }
};