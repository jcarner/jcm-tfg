class chatMsgController {
    constructor() {
        this.flags = {
            sending: true,
            sent: false
        };

        this.avatar = {};


        this.$onInit = () => {
            if (this.msg.type == 'external') {
                this.orientation = 'end';
                let color = ['#ff9d13','#03A9F4','#FF341E'];
                this.avatar = {pos: 3, color: color[Math.ceil(Math.random() * Math.pow(10,9)) % 3]};
                this.orientation = 'end';
                this.msg.fromName ? null : this.msg.fromName = 'Unknown';
                this.cloud = {class: 'external'};
            } else {
                this.orientation = 'start';
                this.avatar = {pos: 1};
                this.msg.fromName == "" ? this.msg.fromName = 'You' : null;
                this.cloud = {class: 'local'};
            }
        };
    }

    test() {
        debugger;
    }
}

chatMsgController.$inject = [];

export {chatMsgController};