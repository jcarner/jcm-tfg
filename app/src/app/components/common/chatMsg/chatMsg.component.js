import template from './chatMsg.html';
import './chatMsg.scss';
import {chatMsgController} from './chatMsg.controller';

export const chatMsgComponent = {
    template,
    controller: chatMsgController,
    bindings: {
        msg: '<'
    }
};