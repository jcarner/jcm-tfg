import angular from 'angular';

import {headerComponent} from './header/header.component';
import {chatMsgComponent} from './chatMsg/chatMsg.component';
import {chatConversationComponent} from './chatConversation/chatConversation.component';
import {chatComponent} from './chat/chat.component';
import {fileComponent} from './file/file.component';
import {folderComponent} from '../common/folder/folder.component';
import {transferComponent} from "./transfer/transfer.component";

import {commonSocial} from './social';

const config = () => {

};

config.$inject = [];

export const common = angular.module('common', [commonSocial.name])
    .config(config)
    .component('header', headerComponent)
    .component('chatMsg', chatMsgComponent)
    .component('chatConversation', chatConversationComponent)
    .component('chat', chatComponent)
    .component('file', fileComponent)
    .component('folder', folderComponent)
    .component('transfer', transferComponent);