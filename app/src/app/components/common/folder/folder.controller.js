import filesTemplate from './folder.files.html';

class folderController {

    constructor($scope, $mdPanel, JSZip, FileSaver) {
        this.$scope = $scope;
        this.$mdPanel = $mdPanel;
        this.JSZip = new JSZip();
        this.FileSaver = FileSaver;

        this.$onInit = () => {
            this.ready = true;
        };

        this.$doCheck = () => {
            if(this.ready) {
                if(this.folder.files.length == 0)
                    { this.close() }
            }
        };
    };

    close() {
        this.onDelete({$id: this.folder.folderId});
    }

    removeFile(file) {
        let i = this.folder.files.indexOf(file);
        if (i >= 0) {
            this.folder.files.splice(i, 1);
        }
    }

    download() {
        let zip = this.JSZip.folder(this.folder.folderId);
        this.folder.files.forEach((file) => {
            // if(file.blob) {
            //     zip.file(file.name, file.blob);
            // } else {
            //     zip.file(file.name, [file.chunks])
            // }
            zip.file(file.name, file.file);
        });
        zip.generateAsync({type: 'blob'})
            .then((content) => {
                this.FileSaver.saveAs(content, this.folder.folderId);
        });
    }

    openFolder() {
        let files = this.folder.files;
        let position = this.$mdPanel.newPanelPosition()
            .relativeTo(document.getElementById(this.folder.folderId))
            .addPanelPosition(
                this.$mdPanel.xPosition.ALIGN_END,
                this.$mdPanel.yPosition.ABOVE
            );
        let config = {
            attachTo: angular.element(document.body),
            locals: {
                files: files,
                columns: 3
            },
            controller: folderFilesController,
            controllerAs: '$ctrl',
            disableParentScroll: this.disableParentScroll,
            template: filesTemplate,
            position: position,
            panelClass: 'files-folder-panel',
            trapFocus: true,
            clickOutsideToClose: true,
            escapeToClose: true,
            focusOnOpen: true,
            zIndex: 150
        };
        this.$mdPanel.open(config);
    }

}

folderController.$inject = ['$scope', '$mdPanel', 'JSZip', 'FileSaver'];

export {folderController};

class folderFilesController {
    constructor() {

    }

    removeFile($id) {
        this.files.forEach((file, i) => {
            if(file.id == $id) {
                this.files.splice(i, 1);
            }
        })
    }
}