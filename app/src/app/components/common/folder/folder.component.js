import template from './folder.html';
import './folder.scss';
import {folderController} from './folder.controller';

export const folderComponent = {
    template,
    controller: folderController,
    bindings: {
        folder: '<',
        onDelete: '&'
    }
};