import template from './transfer.html';
import './transfer.scss';
import {transferController} from "./transfer.controller";

export const transferComponent = {
    template,
    controller: transferController,
    bindings: {
        data: '<',
        onUpdate: '&',
        onDelete: '&'
    }
};