class transferController {
    constructor() {

        this.$onInit = () => {
            if(this.data.name) {
                this.name = this.reduceName(this.data.name);
            }
        }
    }


    reduceName(name) {
        if(name.length > 15) {
            let type = name.split('.');
            return name.slice(0,7) + '... .' + type[type.length-1];
        }
        return name;
    }

    onSwipeRight() {
        this.offset = 30;
    }
    onSwipeLeft() {
        this.offset = 0;
    }

    delete(event) {
        event.preventDefault();
        event.stopPropagation();
        if(this.offset == 30) { this.onDelete({transfer: {id: this.data.id}}) }
    }
}

transferController.$inject = [];

export {transferController};