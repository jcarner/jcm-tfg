import template from './chatConversation.html';
import './chatConversation.scss';
import {chatConversationController} from './chatConversation.controller';

export const chatConversationComponent = {
    template,
    controller: chatConversationController,
    bindings: {
        conversation: '<'
    }
};