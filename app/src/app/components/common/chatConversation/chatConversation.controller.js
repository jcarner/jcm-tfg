
class chatConversationController {
    constructor() {
        this.avatar = {};

        this.$onInit = () => {
            if(!this.conversation.with.img) {
                let color = ['#ff9d13','#03A9F4','#FF341E'];
                this.avatar.color = color[Math.ceil(Math.random() * Math.pow(10,9)) % 3];
            }
        };
    }
}

chatConversationController.$inject =[];

export {chatConversationController};