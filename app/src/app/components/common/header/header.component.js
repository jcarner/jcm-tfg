
import template from './header.html';
import {headerController} from './header.controller';
import './header.scss';

export const headerComponent = {
    template,
    controller: headerController
};