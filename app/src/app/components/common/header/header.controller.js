class headerController {
    constructor($state, $scope) {
        this.$scope = $scope;
        this.currentNavItem = $state.$current.component;
        this.searching = false;
        this.search = {class: 'shrink'};
        this.$state = $state;
    }

    showSearch(event) {
        this.searching = !this.searching;
        // this.search.class == 'shrink' ?
        //     this.search.class = 'stretch' : this.search.class = 'shrink';
    }

    enterRoom() {
        this.$state.transitionTo('app.room', {name: this.searchRoom}, {reload: true,inherit: false, notify: true});
    }
}

headerController.$inject = ['$state', '$scope'];

export {headerController};