import template from './googleProfile.html';
import './googleProfile.scss';
import {googleProfileController} from './googleProfile.controller';

export const googleProfileComponent = {
    template,
    controller: googleProfileController,
    bindings: {
        user: '<'
    }
};