import template from './facebookProfile.html';
import './facebookProfile.scss';
import {facebookProfileController} from './facebookProfile.controller';

export const facebookProfileComponent = {
    template,
    controller: facebookProfileController,
    bindings: {
        user: '<'
    }
};