import angular from 'angular';

import {facebookProfileComponent} from './facebookProfile/facebookProfile.component';
import {googleProfileComponent} from './googleProfile/googleProfile.component';

export const commonSocial = angular.module('common.social', [])
    .component('facebookProfile', facebookProfileComponent)
    .component('googleProfile', googleProfileComponent);