import template from './file.html';
import from './file.scss';
import {fileController} from './file.controller';

export const fileComponent = {
    template,
    controller: fileController,
    bindings: {
        file: '<',
        mode: '<',
        onDelete: '&'
    }
};