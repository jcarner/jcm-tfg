'use strict';

class fileController {

    constructor(FileSaver) {
        this.FileSaver = FileSaver;

        this.$onInit = () => {
            this.file.progress = 0;
        };
        this.$onChanges = (changes) => {}
    }

    getName() {
        if (this.file.name.length > 15) {
            return this.file.name.slice(0,5) + '...' + this.file.name.slice(-5);
        } else {
            return this.file.name;
        }
    }

    parseMetaData() {}

    download() {
        if (this.file.blob) {
            this.FileSaver.saveAs(this.file.blob, this.file.name);
        } else if (this.file.chunks) {
            this.FileSaver.saveAs(new Blob([this.groupChunks()], {type: this.file.type}), this.file.name);
        } else if(this.file.file) {
            this.FileSaver.saveAs(this.file.file, this.file.name);
        }
    }

    delete() {
        this.onDelete({$id: this.file.id});
    }
}

fileController.$inject = ['FileSaver'];

export {fileController};

