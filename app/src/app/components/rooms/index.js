import angular from 'angular';

import {roomsComponent} from './rooms.component';

const config = ($stateProvider) => {
    $stateProvider
        .state('app.rooms', {
            url: '^/rooms',
            component: 'rooms',
            resolve: {
                rooms: [() => {
                    return {name: 'room1'};
                }]
            }
        })
};

config.$inject = ['$stateProvider'];

export const rooms = angular.module('rooms', [])
    .component('rooms', roomsComponent)
    .config(config);