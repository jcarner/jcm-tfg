
import template from './rooms.html';
import './rooms.scss';
import {roomsController} from './rooms.controller';

export const roomsComponent = {
    template,
    controller: roomsController,
    bindings: {
        rooms: '<'
    }
}