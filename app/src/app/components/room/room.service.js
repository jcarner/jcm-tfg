'use strict';
const RoomService = (LoaderService, $window, $rootScope, $q, $http, FileSaver, NotificationService, FileTransferService) => {

        let session = null;
        let playerStreamId = null;
        let publisher = {};

        const setSession = (newSession) => {
            session = newSession;
        };

        const getUserInfo = () => {
            return $http({method: 'GET', url: 'http://freegeoip.net/json/?callback=?'})
                .then((res) => {
                    return JSON.stringify(res, null, 2);
                });
        };

        const getConnectionId = () => {
            return session.connection.id;
        };

        const addUser = (id) => {
            $rootScope.$broadcast('addUser', {connectionId: id});
        };

        const addLocalUser = (id) => {
            publisher.id = id;
            $rootScope.$broadcast('addLocalUser', {id: id});
        };

        const rmUser = (id) => {
            $rootScope.$broadcast('rmUser', {connectionId: id});
            peer.forEach((user, i) => {
                if (user.id === id) {
                    user.pc ? user.pc.close() : null;
                    peer.splice(i, 1);
                }
            });
        };

        const restartRoom = () => {
            peer = [];
            $rootScope.$broadcast('restart');
        };


        let peer = [];
        const findPeer = (connectionId) => {
            let res = null;
            peer.forEach((user) => {
                if (user.id === connectionId) {
                    res = user;
                }
            });
            if (res === null) {
                createPeerConnection(connectionId);
                res = findPeer(connectionId);
            }

            return res;
        };

        // const pc_config = {
        //     iceServers: [{urls: 'stun:23.21.150.121'}, {urls: 'stun:stun.1.google.com:19302'}],
        //     iceTransportPolicy: 'all',
        //     rtcpMuxPolicy: 'negotiate'
        // };
        const pc_config = {
            iceServers: [
                {urls: 'stun:stun.l.google.com:19302'},
                {urls: `turn:${__API_URL__}:55972`, username: `${__TURN_USER__}`, credential: `${__TURN_PASS__}`}
            ],
            iceTransportPolicy: 'all', iceCandidatePoolSize: 0};

        const createPeerConnection = (connectionId) => {
            let i = peer.push({id: connectionId, pc: new RTCPeerConnection(pc_config)});
            let user = peer[i - 1];
            // const pc_constrains = {'optional': [{RtpDataChannels: true}]};
            user.pc.onicecandidate = (event) => {
                if (event.candidate) {
                    console.log('Sending candidate:\n' + event.candidate);
                    session.send({
                        method: 'candidate',
                        sdpMLineIndex: event.candidate.sdpMLineIndex,
                        sdpMid: event.candidate.sdpMid,
                        candidate: event.candidate,
                        to: user.id
                    });
                } else {
                    console.log('All ICE candidates generated');
                    console.log('SDP all candidates:'+user.pc.localDescription.sdp);
                }
            };
            user.pc.onnegotiationneeded = () => {
                console.log('Negotiation needed');
                if (user.pc.signalingState === 'stable') {
                    user.pc.createOffer()
                        .then((offer) => {
                            console.log('Offer generated:\t' + JSON.stringify(offer));
                            return user.pc.setLocalDescription(new RTCSessionDescription(offer));
                        })
                        .then(() => {
                            console.log('Sending offer...');
                            session.send({method: 'offer', to: connectionId, sdp: user.pc.localDescription});
                        })
                        .catch((err) => {
                            console.log('Offer fail' + err);
                        });
                } else {
                    console.log('PeerConnection not in stable state');
                }
            };

            user.pc.ondatachannel = (event) => {
                handleChannel(event.channel);
                switch (event.channel.label) {
                    case 'Room':
                        user.roomDC = event.channel;
                        user.roomDC.onmessage = handleRoomChannel;
                        break;
                    case 'Chat':
                        user.chatDC = event.channel;
                        user.chatDC.onmessage = handleChatChannel;
                        break;
                    case 'Files':
                        user.filesDC = event.channel;
                        user.filesDC.onmessage = handleFileChannel;
                        break;
                    case 'Player':
                        user.playerDC = event.channel;
                        user.playerDC.onmessage = handlePlayerChannel;
                        break;
                }
            };

            user.pc.onclose = () => {
                console.log('PeerConnection with ' + user.id + ' closed');
                LoaderService.error();
                rmUser(connectionId);
                session.send({delete: connectionId, to: ''});
            };

            user.pc.onaddstream = (event) => {
                console.log('Stream added');
                if (event.stream.id === playerStreamId) {
                    $rootScope.$broadcast('playerTrack', event.stream);
                    // let video = document.getElementById('playerMedia');
                    // if (video.srcObject !== event.stream) {
                    //     video.srcObject = event.stream;
                    // }
                } else {
                    $rootScope.$broadcast('addSubscriber', {stream: event.stream, id: user.id});
                }
            };

            user.pc.onaddtrack = (event) => {
                debugger;
            };

            const ICE_log = 'background-color: #aaffd800; color: blue';
            user.pc.onsignalingstatechange = (event) => {
                console.log('%c Connection ' + user.id + ', %c Signaling State changed: %c' + user.pc.signalingState, 'color: red', ICE_log, 'color: green');
            };

            user.pc.oniceconnectionstatechange = (event) => {
                console.log('%c Connection ' + user.id + '%c ICE Connection State changed %c' + user.pc.iceConnectionState, 'color: red', ICE_log, 'color: green');
                if (user.pc.iceConnectionState == 'connected') {
                    if (publisher.stream && publisher.stream !== user.pc.getLocalStreams()[0]) {
                        user.pc.addStream(publisher.stream);
                    }
                    // let streams = user.pc.getLocalStreams();
                    // streams.forEach((stream) => {
                    //     user.pc.addStream(stream);
                    // });
                }
            };

        };

        const createDataChannels = (id) => {
            let user = findPeer(id);
            user.roomDC = user.pc.createDataChannel('Room');
            user.roomDC.onmessage = handleRoomChannel;
            handleChannel(user.roomDC);
            user.filesDC = user.pc.createDataChannel('Files', {protocol: 'TCP'});
            user.filesDC.onmessage = handleFileChannel;
            handleChannel(user.filesDC);
            user.chatDC = user.pc.createDataChannel('Chat');
            user.chatDC.onmessage = handleChatChannel;
            handleChannel(user.chatDC);
            user.playerDC = user.pc.createDataChannel('Player');
            user.playerDC.onmessage = handlePlayerChannel;
            handleChannel(user.playerDC);
        };

        const handleChannel = (channel) => {
            channel.onopen = () => {
                console.log(channel.label + ' Channel OPENED');
            };
            channel.onclose = () => {
                console.log(channel.label + ' Channel CLOSED');
                // peer.forEach((user) => {
                //     channel.label == 'Files' ? createFileChannel(user.id) : createChatChannel(user.id);
                //     sendOffer(user.id);
                // });

            };
            channel.onerror = (err) => {
                console.log(err);
            }
        };


        const handleRoomChannel = (event) => {
            let msg = JSON.parse(event.data);
            switch (msg.method) {
                case 'user':
                    $rootScope.$broadcast('setUser', msg);
                    break;
                case 'player':
                    playerStreamId = msg.streamId;
                    break;
            }
        };

        const handleFileChannel = (event) => {
            if (event.data instanceof Blob) {
                $rootScope.$broadcast('fileContent', {blob: event.data});
            } else if (typeof event.data == "string") {
                let data = JSON.parse(event.data);
                switch (data.type) {
                    case 'meta':
                        $rootScope.$broadcast('fileData', data);
                        break;
                    case 'content':
                        $rootScope.$broadcast('fileContent', data);
                        break;
                }
            }
        };

        const handleChatChannel = (event) => {
            let data = JSON.parse(event.data);
            if (data.to) {
                $rootScope.$broadcast('messageTo', data);
            } else {
                $rootScope.$broadcast('message', data);
            }

        };

        const handlePlayerChannel = (event) => {
            let data = JSON.parse(event.data);
            switch (data.method) {
                case 'meta':
                    $rootScope.$broadcast('playerMeta', data);
                    break;
            }
        };

        const onOffer = (offer) => {
            let user = findPeer(offer.from);
            user.pc.setRemoteDescription(new RTCSessionDescription(offer.sdp))
                .then(() => {
                    return user.pc.createAnswer();
                })
                .then((answer) => {
                    return user.pc.setLocalDescription(new RTCSessionDescription(answer));
                })
                .then(() => {
                    console.log('Sending answer...');
                    session.send({method: 'answer', sdp: user.pc.localDescription, to: user.id});
                })
                .catch((error) => {
                    console.log('Offer: ' + error.message);
                });

            // NotificationService.showFileNotification({name: 'file1'}, {from: 'Josep'}).then(()=>{}, null);
        };

        const onAnswer = (answer) => {
            let user = findPeer(answer.from);
            console.log('Received Answer from ' + answer.from + ':' + answer.sdp);
            user.pc.setRemoteDescription(new RTCSessionDescription(answer.sdp))
                .catch((error) => {
                    console.log('Answer ERROR: ' + error.message);
                });
        };

        const onCandidate = (candidate) => {
            console.log('ICE Candidate received: ' + JSON.stringify(candidate.candidate));
            let user = findPeer(candidate.from);
            user.pc.addIceCandidate(candidate.candidate)
                .catch((err) => {
                    console.log(err.message + '\n' + err.code);
                });
        };

        let progress = {
            file: {rel: 0, total: 0}
        };

        const sendFile = (file, to) => {
            if (to) {
                FileTransferService.sendFile(file, [findPeer(to)], {from: publisher.id});
            } else {
                FileTransferService.sendFile(file, peer, {from: publisher.id, to: 'global'});
            }
        };

        const sendFolder = (files, to) => {
            if (to) {
                FileTransferService.sendFolder(files, [findPeer(to)], publisher.id);
            } else {
                FileTransferService.sendFolder(files, peer, publisher.id, true);
            }
        };

        const sendMessage = (msg) => {
            if (msg.to) {
                let user = findPeer(msg.to);
                msg.from = publisher.id;
                try {
                    user.chatDC.send(JSON.stringify(msg));
                } catch (err) {
                    console.log(`Sending private message to ${msg.to} fails: ${err}`);
                }

            } else {
                peer.forEach((user) => {
                    try {
                        user.chatDC.send(JSON.stringify(msg));
                    } catch (err) {
                        console.log(`Sending public message fails: ${err}`);
                    }

                });
            }
        };

        const sendRoomMsg = (msg) => {
            peer.forEach((user) => {
                try {
                    user.roomDC.send(JSON.stringify(msg))
                } catch (err) {
                    console.log('Sending Room Message Error: ' + error + '\nMessage method: ' + msg.method);
                }
            });
        };

        const sendFileMsg = (msg) => {
            msg.from = publisher.id;
            try {
                if (msg.to) {
                    let to = findPeer(msg.to);
                    to.filesDC.send(JSON.stringify(msg));
                } else {
                    peer.forEach((user) => {
                        user.filesDC.send(JSON.stringify(msg));
                    });
                }
            } catch (err) {
                console.log('Sending File Message Error: ' + err);
            }
        };

        const sendPlayerMsg = (msg) => {
            msg.from = publisher.id;
            try {
                peer.forEach((user) => {
                    user.playerDC.send(JSON.stringify(msg));
                });
            } catch (err) {
                console.log('Sending Player Message Error:' + err);
            }
        };

        const publish = (stream) => {
            if (stream) {
                publisher.stream = stream;
            }

            peer.forEach((user) => {
                user.pc.addStream(publisher.stream);
            });
        };


        const broadcastStream = (stream) => {
            sendRoomMsg({method: 'player', streamId: stream.id});
            peer.forEach((user) => {
                try {
                    user.pc.addStream(stream);
                    //user.pc.addTrack(track, stream);
                } catch (err) {
                    console.log('Broadcast stream failed: ' + stream.label);
                }
            });
            // stream.getTracks().forEach((track) => {
            //     peer.forEach((user) => {
            //         try {
            //             user.pc.addStream(stream);
            //             //user.pc.addTrack(track, stream);
            //         } catch (err) {
            //             console.log('Broadcast stream failed: ' + stream.label);
            //         }
            //     });
            // });
        };

        const stopBroadcast = (stream) => {
            peer.forEach((user) => {
                try {
                    user.pc.removeStream(stream);
                } catch(err) {
                    console.log('Stop broadcast failed: ' + err);
                }
            });
        };

        return {
            setSession,
            getUserInfo,
            getConnectionId,

            addLocalUser,
            addUser,
            rmUser,
            restartRoom,

            createPeerConnection,
            createDataChannels,
            onOffer,
            onAnswer,
            onCandidate,

            sendMessage,
            sendRoomMsg,
            sendPlayerMsg,

            sendFile,
            sendFolder,

            publish,
            broadcastStream,
            stopBroadcast
        };
    }
;

RoomService.$inject = ['LoaderService', '$window', '$rootScope', '$q', '$http', 'FileSaver', 'NotificationService', 'FileTransferService'];

export {RoomService};