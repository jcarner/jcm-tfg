
import template from './room.html';
import './room.scss';
import {roomController} from './room.controller';

export const roomComponent = {
    template,
    controller: roomController,
    bindings: {}
};