'use strict';

class fileNavController {
    constructor($scope, RoomService) {
        this.$scope = $scope;
        this.RoomService = RoomService;

        this.size = 15;
        this.class = '';
        this.icon = { size: 'forward' };

        this.$scope.$on('addFile', this.onAddFile);
        this.$scope.$on('transferProgressUpdate', this.onProgressUpdate);
        this.$scope.$on('cancelTransfer', this.onCancelTransfer);

        this.$onInit = () => {
            let element = document.getElementById('fileNavContainer');
            element.addEventListener('drop', (event) => {
                event.preventDefault();
                event.stopPropagation();
                this.class = '';
                if (event.dataTransfer.files.length === 1) {
                    let file = event.dataTransfer.files[0];
                    //this.files.files.push({name: file.name, file: file});
                    this.sendFile(file);
                } else if (event.dataTransfer.files.length > 1) {
                    let files = event.dataTransfer.files;
                    //this.files.folders.push({type: 'folder', files: files});
                    this.sendFolder(files);
                }
                this.$scope.$digest();
            }, false);
            element.addEventListener('dragover', (event) => {
                event.preventDefault();
                this.class = 'drag-on';
            }, false);
            element.addEventListener('dragleave', (event) => {
                event.preventDefault();
                this.class = '';
            }, false);
            element.addEventListener('dragend', (event) => {
                event.preventDefault();
                this.class = '';
            }, false);

            let x = 0, down = false;
            const k = 0.3;
            element.addEventListener('mousedown', (event) => {
                down = true;
                x = event.pageX;
            }, false);
            element.addEventListener('mouseup', (event) => {
                down = false;
            }, false);
            element.addEventListener('mouseout', (event) => {
                // down = false;
            }, false);
            element.addEventListener('mousemove', (event) => {
                if (down) {
                    element.scrollLeft += Math.floor((event.pageX - x) * k);
                }
            }, false);
        };
    }

    sendFile(file) {
        this.RoomService.sendFile(file);
    }

    sendFolder(files) {
        this.RoomService.sendFolder(files);
    }

    toggleSize() {
        if (this.size === 100) {
            this.size = 15;
            this.class = '';
            this.icon.size = 'forward';
            this.showFiles = false;
        } else {
            this.size = 100;
            this.icon.size = 'back';
            this.class += ' full';
            this.showFiles = true;
        }
    }

    selectFile() {
        let input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.onchange = (event) => {
            if(event.target.files.length == 1) {
                this.sendFile(event.target.files[0]);
            } else if (event.target.files.length > 1) {
                this.sendFolder(event.target.files);
            }
        };
        let event = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        });
        input.dispatchEvent(event);

    }

    removeFile(id) {
        this.files.files.forEach((file, i) => {
            if(file.id == id) {
                this.files.files.splice(i, 1);
            }
        });
    }

    removeFolder(id) {
        this.files.files.forEach((folder, i) => {
            if(folder.folderId == id) {
                this.files.files.splice(i, 1);
            }
        })
    }


    findFolder(id) {
        let res = null;
        this.files.files.forEach((folder) => {
            if(folder.folderId == id) {
                res = folder;
            }
        });
        if(res == null) {
           this.files.files.push({type: 'folder', folderId: id, files: []});
           return this.findFolder(id);
        }
        return res;
    }

    findFile(id) {
        let res = null;
        this.files.files.forEach((file) => {
            switch(file.type) {
                case 'folder':
                    file.files.forEach((ffile) => {
                        if(ffile.id == id) {
                            res = ffile;
                            return;
                        }
                    });
                    break;
                default:
                    if(file.id == id) {
                        res = file;
                        return;
                    }
            }

        });
        return res;
    }

    onAddFile(event, data) {
        if(data.to != 'global') { return; }
        let self = event.currentScope.$ctrl;
        data.progress = 0;
        if(data.folderId){
            let folder = self.findFolder(data.folderId);
            folder.files.push(data);
        } else {
            self.files.files.push(data);
        }

    }

    onProgressUpdate(event, data) {
        if(!data.isGlobal) { return }
        let self = event.currentScope.$ctrl;
        let file = self.findFile(data.id);
        if(data.total) { file.total = data.total }
        file.progress += (data.progress / file.total) * 100;
        if(data.last) { file.progress = 100 }

    }

    onCancelTransfer(event, data) {
        if(!data.isGlobal) { return }
        let self = event.currentScope.$ctrl;
        //todo notifify the error
    }

    updateFileNav() {
        this.onUpdate({fileNav: {}});
    }

}

fileNavController.$inject = ['$scope', 'RoomService'];

export {fileNavController};