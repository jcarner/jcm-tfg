import template from './fileNav.html';
import './fileNav.scss';
import {fileNavController} from './fileNav.controller';

export const fileNavComponent = {
    template,
    controller: fileNavController,
    bindings: {
        files: '=',
        onUpdate: '&'
    }
};