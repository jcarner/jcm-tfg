'use strict';

const FileTransferService = ($rootScope) => {

    let chunkLength = 16300;

    const sendFile = (file, to, meta) => {

        let meta1 = {};
        Object.assign(meta1, meta, {
            type: 'meta',
            id: (0 | Math.random() * 9e16).toString(36),
            name: file.name,
            MIMEtype: file.type,
            size: file.size
        });

        $rootScope.$broadcast('addFile', {
            id: meta1.id, name: file.name,
            folderId: meta.folderId || null,
            MIMEtype: file.type, size: file.size,
            file: file, to: meta1.to
        });

        $rootScope.$broadcast('addTransfer', { id: meta1.id, name: file.name });

        console.log("Sending file...\nid: " + meta1.id);

        to.forEach((peer) => {
            try {
                peer.filesDC.send(JSON.stringify(meta1));
            } catch(err) {
                console.log('Error sending file metadata to ' + peer.id + ' : ' + err);
            }
        });

        let meta2 = {};
        Object.assign(meta2, meta, {id: meta1.id});
        // while (file.size / chunkLength > 1000 || chunkLength*5 < 100000) {
        //     chunkLength = chunkLength * 5;
        // }
        let reader = new FileReader();
        reader.onload = () => {chunkAndSend(event, null, meta2, to)};
        reader.readAsDataURL(file);
    };

    const sendFolder = (files, to, from, isGlobal) => {
        let meta = {
            folderId: (0 | Math.random() * 9e16).toString(36),
            from: from
        };

        if(isGlobal) {
            meta.to = 'global'
        }

        for(let i = 0; i < files.length; i++) {
            sendFile(files[i], to, meta);
        }
    };

    const chunkAndSend = (event, next, meta, to) => {
        let msg = {type: 'content'};
        Object.assign(msg, msg, meta);
        if (event) {
            next = event.target.result;
            //initialize transfer progress
            updateProgress({
                id: msg.id, progress: 0,
                total: next.length,
                isGlobal: meta.to == 'global' ? true : false});
        }

        if (next.length > chunkLength) {
            Object.assign(msg, msg, {chunk: next.slice(0, chunkLength)});
        } else {
            console.log('ChunkAndSend complete: sending last chunk of msg ' + msg.id);
            Object.assign(msg,msg, {chunk: next, last: true});
        }

        //send chunk
        to.forEach((peer) => {
            try {
                peer.filesDC.send(JSON.stringify(msg));
            } catch (err) {
                cancelTransfer(msg, to);
                console.log('Error sending file chunk to ' + peer.id + ' : ' + err);
            }

        });

        //update transfer progress
        if(msg.last) {
            updateProgress({id: msg.id, last: true, isGlobal: meta.to == 'global' ? true : false});
        } else {
            updateProgress({
                id: msg.id, progress: msg.chunk.length,
                isGlobal: meta.to == 'global' ? true : false
            });
        }

        //send remain chunks
        let remain = next.slice(msg.chunk.length);
        if (remain.length) {
            setTimeout(() => {
                chunkAndSend(null, remain, meta, to);
            }, 50);
        }
    };

    const updateProgress = (data) => {
        $rootScope.$broadcast('transferProgressUpdate', data);
    };

    const cancelTransfer = (transfer, to) => {
        $rootScope.$broadcast('cancelTransfer', {
            id: transfer.id, to: to.id,
            isGlobal: transfer.isGlobal ? true : false
        });
    };

    return {
        sendFile,
        sendFolder
    }
};

FileTransferService.$inject = ['$rootScope'];

export {FileTransferService};