class userListController {
    constructor($mdSidenav, $scope) {
        this.$mdSidenav = $mdSidenav;
        this.$scope = $scope;
        this.publishers = $scope.$parent.$ctrl.users.publishers;
        this.subscribers = $scope.$parent.$ctrl.users.subscribers;

    }

    randomColor() {
        let color = ['#63bf6a','#ff9d13','#03A9F4','#FF341E'];
        return color[Math.ceil(Math.random() * Math.pow(10,9)) % 4];
    }

    show(connId) {
        this.$scope.$parent.$ctrl.showUser(connId);
    }

}

userListController.$inject = ['$mdSidenav', '$scope'];

export {userListController};