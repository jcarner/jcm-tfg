import template from './userList.html';
import './userList.scss';
import {userListController} from './userList.controller';


export const userListComponent = {
    template,
    controller: userListController
};