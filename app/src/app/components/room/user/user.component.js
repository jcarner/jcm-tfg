import template from './user.html';
import './user.scss';
import {userController} from './user.controller';

export const userComponent = {
    template,
    controller: userController,
    bindings: {
        stream: '<',
        publisher: '<',
        user: '<',
        onNewMsg: '&',
        onUpdate: '&'
    }
};