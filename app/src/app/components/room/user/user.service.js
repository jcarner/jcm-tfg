
const UserService = () => {
    let user = null;

    const setUser = (newUser) => {
        user = newUser;
    };

    return {
        setUser
    }
};

UserService.$inject = [];

export {UserService};