class userController {
    constructor(RoomService, $scope, SessionService, UserService) {
        this.$scope = $scope;
        this.RoomService = RoomService;
        this.SessionService = SessionService;
        this.UserService = UserService;

        this.id = `user-${this.stream}`;
        this.show = {profile: true, chat: false, files: false, detail: false, facebook: false, google: false};
        this.class = {icon: {user: '', video: '', audio: '', detail: ''}};
        this.on = {video: false, audio: false};
        this.chatInput = {};

        this.UserService.setUser(this.user);

        this.localUser = this.$scope.$parent.$ctrl.users.publishers[0];

        this.$onChanges = (changes) => {
            this.$scope.$parent.$ctrl.findUser(this.user.connectionId).name = this.name;
            if(changes.stream && !changes.stream.isFirstChange()) {
                let video = document.getElementById(this.user.streamId);
                if(video.srcObject !== this.user.stream) {
                    video.srcObject = this.user.stream;
                }
            }
        };

        let element = document.getElementById('userContainer');
        element.addEventListener('dragstart', (event) => {
            event.preventDefault();
        }, false);
        element.addEventListener('dragenter', (event) => {
            event.preventDefault();
        }, false);
        element.addEventListener('dragend', (event) => {
            event.preventDefault();
        }, false);
        element.addEventListener('dragleave', (event) => {
            event.preventDefault();
        }, false);
        element.addEventListener('drag', (event) => {
            event.preventDefault();
        }, false);
        element.addEventListener('drop', (event) => {
            event.preventDefault();
            if (event.dataTransfer.files.length == 1) {
                this.RoomService.sendFile(event.dataTransfer.files[0], this.user.connectionId);
            } else if (event.dataTransfer.files.length > 1) {
                this.RoomService.sendFolder(event.dataTransfer.files, this.user.connectionId);
            }
        }, false);
    }

    openMenu($mdOpenMenu, event) {
        $mdOpenMenu(event);
    }

    toggleVideo() {
        this.on.video = !this.on.video;
        if (this.user.subscriber !== undefined) {
            this.user.subscriber.subscribeToVideo(this.on.video);
        }
        if (this.user.publisher !== undefined) {
            this.user.publisher.publishVideo(this.on.video);
        }
        this.on.video ? this.class.icon.video = '' : this.class.icon.video = 'red';
    }

    toggleAudio() {
        this.on.audio = !this.on.audio;
        if (this.user.subscriber !== undefined) {
            this.user.subscriber.subscribeToAudio(this.on.audio);
        }
        if (this.user.publisher !== undefined) {
            this.user.publisher.publishAudio(this.on.audio);
        }
        this.on.audio ? this.class.icon.audio = '' : this.class.icon.audio = 'red';
    }

    toggleDetail() {
        this.show.detail = !this.show.detail;
        this.show.detail ? this.class.icon.detail = 'yellow' : this.class.icon.detail = '';
    }

    showProfile() {
        Object.assign(
            this.show,
            this.show,
            {profile: true, chat: false, files: false, facebook: false, google: false}
        );
        this.show.detail = true;
    }

    showChat() {
        Object.assign(
            this.show,
            this.show,
            {chat: true, profile: false, files: false, facebook: false, google: false}
        );
        this.show.detail = true;
        if (this.show.chat) {
            this.onUpdate({changes: {target: 'chat', id: this.user.connectionId}});
        }
    }

    showFiles() {
        Object.assign(
            this.show,
            this.show,
            {chat: false, profile: false, files: true, facebook: false, google: false}
        );
        this.show.detail = true;
    }

    changeShow(key) {
        for (let k in this.show) {
            if (k != 'detail') {
                this.show[k] = false;
            }
        }
        this.show[key] = true;
        this.show.detail = true;
    }


    chat(event) {
        let me = this.$scope.$parent.$ctrl.users.publishers[0];
        let msg = {
            to: this.user.connectionId,
            content: event.currentTarget.value
        };

        this.RoomService.sendMessage(msg);
        msg.fromName = 'You';
        this.user.chatContent.push(msg);
        this.msg = '';
    }

    send() {
        if (!this.msg || this.msg === '') {
            return;
        }
        let me = this.$scope.$parent.$ctrl.users.publishers[0];
        let msg = {
            to: this.user.connectionId,
            content: this.msg,
            fromName: me.name,
            fromId: me.connectionId,
            photo: me.photo ? me.photo : null,
            type: 'start'
        };
        this.user.chatContent.push(msg);
        //reset the input container
        this.chatInput.height = 0;
        this.msg = '';

        this.RoomService.sendMessage(msg);
        this.onNewMsg({id: this.user.connectionId});
    }

    checkMsg(event) {
        if (event.keyCode == 13) {
            if (this.msg == '') {
                event.preventDefault();
            } else if(event.shiftKey || event.ctrlKey) {
                event.preventDefault();
                this.send();
            } else {
                let chatInput = event.currentTarget;
                if (chatInput.scrollHeight > chatInput.clientHeight) {
                    //stretch the input container
                    this.chatInput.height = chatInput.scrollHeight + 15;
                }
            }
        }
    }
}

userController.$inject = ['RoomService', '$scope', 'SessionService', 'UserService'];

export {userController};