'use strict';

class playerController {
    constructor($scope, $timeout, PlayerService) {
        this.self = this;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.PlayerService = PlayerService;

        this.media = null;
        this.list = {audio: [], video: []};
        this.previousState = null;

        //initialize
        this.mode = 'audio';
        this.paused = true;
        this.reduced = true;
        this.orientation = 'row';
        this.width = 33, this.height = null;
        this.history = true;
        this.showList = false;

        this.$scope.$on('playerTrack', this.onTrack);
        this.$scope.$on('playerMeta', this.onPlayerMeta);

        this.$onInit = () => {
            this.mode = 'audio';
            let mediaElement = document.getElementById("playerMedia");
            mediaElement.addEventListener('play', () => {
                if (!this.remote) {
                    this.PlayerService.sendMediaMeta({
                        method: 'meta',
                        type: this.media.type,
                        name: this.media.name,
                        meta: this.media.meta,
                        max: this.max,
                        maxT: this.maxT,
                    });
                    this.mediaElement.captureStream = this.mediaElement.captureStream || this.mediaElement.mozCaptureStream;
                    this.PlayerService.broadcastStream(this.mediaElement.captureStream());
                }
                this.$scope.$apply(() => {
                    this.paused = false;
                    this.history = this.media.type != 'video';
                });
            });
            mediaElement.addEventListener('pause', () => {
                this.$scope.$apply(() => {
                    this.paused = true;
                });
            });

            this.mediaElement = mediaElement;
            this.mediaElement.onloadeddata = () => {
                let duration = this.remote ? this.max : this.mediaElement.duration;
                let maxS = Math.floor(duration % 60) <= 9 ?
                    '0' + Math.floor(duration % 60) : Math.floor(duration % 60);
                this.maxT = `${Math.floor(duration / 60)}:${maxS}`;
                this.max = Math.floor(duration);
                this.volume = this.mediaElement.volume * 100;
                this.history = this.media.type != 'video';
                this.update();
            };
            this.mediaElement.ontimeupdate = () => {
                this.$scope.$apply(() => {
                    let cS = Math.floor(this.mediaElement.currentTime % 60) <= 9 ?
                        '0' + Math.floor(this.mediaElement.currentTime % 60) : Math.floor(this.mediaElement.currentTime % 60);
                    this.currentT = `${Math.floor(this.mediaElement.currentTime / 60)}:${cS}`;
                    this.progress = Math.floor(this.mediaElement.currentTime);
                });
            };
            //handle file input events
            let player = document.getElementById('playerContainer');
            player.addEventListener('drop', (event) => {
                event.preventDefault();
                this.processFiles(event.dataTransfer.files);
                this.reduced = false;
                this.orientation == 'row' ? this.width = 55 : null;
            });

            this.update();
        };

        this.$doCheck = () => {
            // if(this.history) {
            //     this.volumeHeight = document.getElementById('playerHistoryContainer').clientHeight + 'px'
            // }
            // if(this.mediaElement.clientHeight > 0) {
            //     this.volumeHeight = this.mediaElement.clientHeight  + 'px'
            // }

            let container = document.getElementById('playerContainer');
            this.height = Math.round(100 * (container.clientHeight + container.offsetTop) / window.innerHeight);
            this.volumeHeight = `calc(${container.clientHeight}px - ${this.reduced ? '15vh':'20vh'})`;
            let currentState = {orientation: this.orientation, width: this.width, height: this.height};
            if(JSON.stringify(this.previousState) != JSON.stringify(currentState)) {
                this.update();
            }

            let list = document.getElementsByClassName('list')[0];
            let history = document.getElementsByTagName('player-history')[0];
            if(list && history && !this.reduced) {
                let max = Math.max(list.scrollHeight, history.scrollHeight);
                this.contentHeight = Math.max(list.scrollHeight, history.scrollHeight) + 'px';
            }

        };
        this.$postLink = () => {
        };
    }

    processFiles(files) {
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            if (file.type.match('audio') !== null) {
                let audio = null;
                let element = new Audio(URL.createObjectURL(file));
                element.onloadedmetadata = () => {
                    audio = {name: file.name.split('.')[0], file: file, duration: element.duration, type: 'audio'};
                    if (file.name.match('mp3')) {
                        this.PlayerService.decodeMp3(file)
                            .then((res) => { audio.meta = res.meta });
                    }
                    this.list.audio.push(audio);
                };
                this.changeMode('audio');
            } else if (file.type.match('video')) {
                let video = null;
                let element = document.createElement('video');
                element.src = URL.createObjectURL(file);
                element.onloadedmetadata = () => {
                    video = {name: file.name.split('.')[0], file: file, meta: '', duration: element.duration, type: 'video'};
                    this.PlayerService.getThumbnail(file)
                        .then((res) => { video.thumbnail = res.src });
                    video.meta = {name: video.name, duration: video.duration, type: 'video'};
                    this.list.video.push(video);
                };
                this.changeMode('video');
            }
        }
        this.reduced = false;
    }

    selectFiles() {
        let input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'video/*,audio/*');
        input.onchange = (event) => {
            this.processFiles(event.target.files);
        };
        let event = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        });
        input.dispatchEvent(event);
    }

    selectMedia(media) {
        this.media = media;
        this.remote = false;
        this.mediaElement.src = URL.createObjectURL(media.file);
        this.list.audio.forEach((el) => {
            el.class = ''
        });
        this.list.video.forEach((el) => {
            el.class = ''
        });
        media.class = 'selected';

    }

    setPlayerTime() {
        this.mediaElement.currentTime = this.progress;
    }

    nextMedia() {
        let list = null, next = null;
        switch (this.mode) {
            case 'audio':
                list = this.list.audio;
                break;
            case 'video':
                list = this.list.video;
                break;
        }

        list.forEach((media, i) => {
            if (media.class == 'selected') {
                let nextIdx = (i + 1) % list.length;
                next = list[nextIdx];
                return null;
            }
        });

        if(next == null) { return list[0]; }
        this.selectMedia(next);
    }

    previousMedia() {
        let list = this.media.type == 'video' ? this.list.video : this.list.audio;
        let previousIdx = (list.indexOf(this.media) + list.length - 1) % list.length;
        this.selectMedia(list[previousIdx]);
    }

    onTrack(event, stream) {
        let self = event.currentScope.$ctrl;
        self.remote = true;
        let video = document.getElementById('playerMedia');
        if (video.srcObject !== stream) {
            video.srcObject = stream;
        }
    }


    onPlayerMeta(event, data) {
        let self = event.currentScope.$ctrl;
        self.maxT = data.maxT;
        self.max = data.max;
        self.media = data;
    };

    setVolume(vol) {
        if (vol == 0 || vol) {
            this.mediaElement.volume = vol / 100;
            this.volume = vol;
        } else {
            this.mediaElement.volume = this.volume / 100;
        }
    }

    toogleList() {
        this.showList = !this.showList;
    }

    togglePlay() {
        if (this.paused) {
            this.remote = false;
            this.mediaElement.play();
        } else {
            this.mediaElement.pause();
        }
    }

    toggleView() {
        this.reduced = !this.reduced;
        this.width = this.reduced ? 33 : 55;

        switch(true) {
            case this.reduced:
                this.contentHeight = 0+'px';
                this.history = !this.media || this.media.type != 'video';
                this.showList = false;
                break;
            case !this.reduced:
                this.listDetails = !this.history && (!this.media || this.media.type != 'video');
                this.showList = !this.history || !this.media || this.media.type != 'video';
                break;
        }
    }

    toggleHistory() {
        this.history = !this.history;
        this.listDetails = !this.history && (!this.media || this.media.type != 'video');
        this.showList = !this.history || !this.media || this.media.type != 'video';
    }

    changeMode(mode) {
        if(this.mode == mode && this.showList) {
            this.showList = false;
            return;
        }
        this.mode = mode;
        this.showList = true;
        this.history = !this.media || this.media.type != 'video';
    }

    //deprecated
    toggleOrientation() {
        if (this.orientation == 'row') {
            this.orientation = 'column';
            this.height = 100;
            this.width = 33;
        } else {
            this.orientation = 'row';
            this.width = 55;
        }
    }

    update() {
        this.previousState = {orientation: this.orientation, width: this.width, height: this.height};
        this.onUpdate({player: {width: this.width, height: this.height, orientation: this.orientation}});
    }

    updateHistory(history) {
        if (history.show) {
            if (history.show == true) {
                this.history = true;
            } else {
                this.history = false;
            }
        }
    }

}

playerController.$inject = ['$scope', '$timeout', 'PlayerService'];

export {playerController};