class clipController {
    constructor() {

        this.$onInit = () => {
            let maxS = Math.floor(this.data.duration%60);
            this.duration = `${Math.floor(this.data.duration/60)}:${maxS <= 9 ? '0' + maxS : maxS}`;
        };
    }
}

clipController.$inject = [];

export {clipController};