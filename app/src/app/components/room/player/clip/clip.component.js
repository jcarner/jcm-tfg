import template from './clip.html';
import './clip.scss';
import {clipController} from "./clip.controller";


export const clipComponent = {
    template,
    controller: clipController,
    bindings: {
        data: '<',
        detail: '<'
    }
};