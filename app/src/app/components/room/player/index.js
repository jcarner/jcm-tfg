import angular from 'angular';

import {playerComponent} from './player.component';
import {songComponent} from "./song/song.component";
import {clipComponent} from "./clip/clip.component";
import {playerHistoryComponent} from "./playerHistory/playerHistory.component";

import {PlayerService} from './player.service';

export const player = angular.module('player', [])
    .component('player', playerComponent)
    .component('song', songComponent)
    .component('clip', clipComponent)
    .component('playerHistory', playerHistoryComponent)
    .factory('PlayerService', PlayerService);