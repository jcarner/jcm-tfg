class playerHistoryController {
    constructor($interval) {
        this.$interval = $interval;

        this.nowSettings = {padding: 0, order: 0};
        this.history = [];

        this.$onInit = () => {
            this.showHistory = false;
            let nowContainer = document.getElementById('nowContainer');
            let target = 0;
            this.$interval(() => {
                if(this.now) {
                    nowContainer.scrollLeft = (nowContainer.scrollLeft + 5);
                    let tag = nowContainer.children[target];
                    if(nowContainer.scrollLeft >= tag.offsetWidth) {
                        this.nowSettings.order = (this.nowSettings.order + 2) % 3;
                        target = (target + 1) % 3;
                        // tag.style.paddingRight = 50 + Math.max(0, nowContainer.children[target].clientWidth - tag.clientWidth) + 'px';
                        nowContainer.scrollLeft = 0;
                    }
                }
            }, 50);
        };
        this.$onChanges = (changes) => {
            if(!changes.now.isFirstChange()) {
                changes.now.currentValue.cTime = new Date();
                this.history.push(changes.now.currentValue);
                let nowContainer = document.getElementById('nowContainer');

                let max = 0;
                [...nowContainer.children].forEach((span) => {
                    max = Math.max(max, span.clientWidth);
                });
                [...nowContainer.children].forEach((span) => {
                    span.style.paddingRight = (max / 2) + 'px';
                    span.style.paddingLeft = (max / 2) + 'px';
                });
            }
        }
    }

    toggleHistory() {
        this.showHistory = !this.showHistory;
    }

    timeSince(since) {
        return Math.floor((new Date() - since)/(1000*60)) + ' minutes ago';
    }
}

playerHistoryController.$inject = ['$interval'];

export {playerHistoryController};