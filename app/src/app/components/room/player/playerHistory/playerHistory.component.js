import template from './playerHistory.html';
import './playerHistory.scss';
import {playerHistoryController} from "./playerHistory.controller";

export const playerHistoryComponent = {
    template,
    controller: playerHistoryController,
    bindings: {
        now: '<',
        onUpdate: '&'
    }
};