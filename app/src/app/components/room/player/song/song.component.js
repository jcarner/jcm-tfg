import template from './song.html';
import './song.scss';
import {songController} from "./song.controller";

export const songComponent = {
    template,
    controller: songController,
    bindings: {
        data: '<',
        onSelect: '&'
    }
};