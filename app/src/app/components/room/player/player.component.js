import template from './player.html';
import './player.scss';
import {playerController} from './player.controller';

export const playerComponent = {
    template,
    controller: playerController,
    bindings: {
        onUpdate: '&'
    }
};