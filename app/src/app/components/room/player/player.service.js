'use strict';

const PlayerService = (RoomService, $rootScope, $q) => {

    let playerStream = null;

    let mp3Parser = document.createElement('script');
    mp3Parser.src = './app/components/room/player/id3-minimized.js';
    document.getElementsByTagName('script')[0].parentNode.appendChild(mp3Parser);

    let adapter = document.createElement('script');
    adapter.src = './app/components/room/player/adapter.js';
    document.getElementsByTagName('script')[0].parentNode.appendChild(adapter);

    const decodeMp3 = (file) => {
        let defer = $q.defer();
        if (ID3) {
            ID3.loadTags(file.name, () => {
                defer.resolve({meta: ID3.getAllTags(file.name)});
            }, {
                dataReader: ID3.FileAPIReader(file),
                onError: (err) => {
                    console.log(err);
                    defer.reject();
                }
             })
        } else {
            console.log('MP3 API not loaded');
        }
        return defer.promise;
    };

    const getThumbnail = (file) => {
        let defer = $q.defer();
        let canvas = document.createElement('canvas');
        let video = document.createElement('video');
        video.src = URL.createObjectURL(file);
        video.ontimeupdate = () => {
            let context = canvas.getContext('2d');
            context.drawImage(video, 0, 0, canvas.width, canvas.height);
            let img = new Image();
            img.src = canvas.toDataURL();
            if(video.currentTime > 0) defer.resolve(img);
        };
        video.onloadedmetadata = () => {
            canvas.height = video.videoHeight;
            canvas.width = video.videoWidth;
            video.currentTime = Math.round(video.duration*0.5);
        };

        return defer.promise;
    };


    const pause = () => {
        peer.forEach((user) => {
            console.log('removeTrack:' + user.id);
            //remove tracks of the PeerConection
            user.pc.getSenders().forEach((sender) => {
                if (user.pc.removeTrack) {
                    user.pc.removeTrack(sender);
                } else {
                    user.pc.getLocalStreams().forEach((stream) => {
                        user.pc.removeStream(stream);
                    });
                    user.pc.getRemoteStreams().forEach((stream) => {
                        user.pc.removeStream(stream);
                    });
                }

            });
        });
    };

    const playerConstrains = {optional: [], mandatory: {OfferToReceiveAudio: true, OfferToReceiveVideo: true}};

    const broadcastStream = (stream) => {
        playerStream = stream;
        RoomService.broadcastStream(stream);
        // peer.forEach((user) => {
        //     //user.pc.addStream(stream);
        //     if (user.pc.getSenders()) {
        //         pause();
        //     }
        //     if (user.pc.addTrack) {
        //         stream.getTracks().forEach((track) => {
        //             user.pc.addTrack(track, stream);
        //         });
        //     }
        //     console.log('addStream:' + user.id);
        // });
    };

    const sendMediaMeta = (meta) => {
        RoomService.sendPlayerMsg(meta);
    };


    return {
        decodeMp3,
        getThumbnail,
        pause,
        broadcastStream,
        sendMediaMeta
    };
};

PlayerService.$inject = ['RoomService', '$rootScope', '$q'];

export {PlayerService};