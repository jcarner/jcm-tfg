import template from './localUser.html';
import './localUser.scss';
import {localUserController} from './localUser.controller';

export const localUserComponent = {
    template,
    controller: localUserController,
    bindings: {
        stream: '<',
        publisher: '=',
        user: '=',
        onUpdate: '&'
    }
};