
const LocalUserService = () => {
    let user = null;

    const setUser = (newUser) => {
        user = newUser;
    };

    return {
        setUser
    }
};

LocalUserService.$inject = [];

export {LocalUserService};