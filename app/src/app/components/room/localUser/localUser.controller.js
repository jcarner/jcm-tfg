class localUserController {
    constructor(RoomService, $scope, SessionService, UserService, FacebookService, GoogleService) {
        this.self = this;
        this.$scope = $scope;
        this.RoomService = RoomService;
        this.SessionService = SessionService;
        this.UserService = UserService;
        this.FacebookService = FacebookService;
        this.GoogleService = GoogleService;

        this.id = `user-${this.stream}`;
        this.show = {profile: true, chat: false, files: false, detail: false};
        this.class = {
            icon: {user: '', video: '', audio: '', detail: ''},
            menu: ''
        };
        this.on = {video: true, audio: true};

        this.UserService.setUser(this.user);

        this.$scope.$on('addUser', this.onNewUser);

        this.$onInit = () => {};
    }

    openMenu($mdOpenMenu, event) {
        $mdOpenMenu(event);
    }

    getUserMedia(options) {
        let self = this.self;
        navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia;
        let constraints = {
            audio: options ? options.audio : true,
            video: options && !options.video ? false : { width: 1280, height: 720 }
        };
        if (navigator.getUserMedia) {
            let video = document.getElementById(this.id);
            self.video = video;
            navigator.getUserMedia(constraints,
                (stream) => {
                    self.options = options ? options : { audio: true, video: true };
                    video.srcObject = stream;
                    video.onloadedmetadata = (evt) => {
                        video.play();
                        self.class.icon.video = self.options.video ? 'red' : '';
                        self.class.icon.audio = self.options.audio ? 'red' : '';
                        self.RoomService.publish(stream);
                    };

                },
                (err) => {
                    self.publisher = false;
                    console.log("The following error occurred: " + err.name);
                }
            );
            self.publisher = true;
        } else {
            console.log("getUserMedia not supported");
        }
    }

    mute(off) {
        let stream = this.video.srcObject;
        stream.getTracks().forEach((track) => {
            if(track.kind == 'audio') {
                track.enabled = !off;
            }
        });
    }



    toggleVideo() {
        if(this.video) {
            this.options.video = !this.options.video;
            let stream = this.video.srcObject;
            stream.getTracks().forEach((track) => {
               if(track.kind == "video") {
                   track.enabled = this.options.video;
               }
            });
            this.class.icon.video = this.options.video ? 'red' : '';
        } else {
            this.getUserMedia();
        }
    }

    toggleAudio() {
        if(this.video) {
            this.options.audio = !this.options.audio;
            this.options.audio ? this.class.icon.audio = 'red' : this.class.icon.audio = '';
            this.mute(!this.options.audio);
        }
    }

    toggleDetail() {
        this.show.detail = !this.show.detail;
        if (this.show.detail) {
            this.class.icon.detail = 'yellow';
            this.onUpdate({localUser: {$reduce: false}});
        } else {
            this.class.icon.detail = '';
            this.onUpdate({localUser: {$reduce: true}});
        }
    }

    showProfile() {
        this.show = {profile: true, chat: false, files: false};
        this.toggleDetail();
    }

    showChat() {
        this.show = {chat: true, profile: false, files: false};
        this.toggleDetail();
    }

    showFiles() {
        this.show = {chat: false, profile: false, files: true};
        this.user.notify.files = 0;
        this.class.menu = '';
        this.toggleDetail();
    }

    authenticate(provider) {
        if (provider === 'facebook') {
            this.FacebookService.getMe()
                .then((res) => {
                    this.user.name = res.name;
                    this.user.birthday = res.birthday;
                    this.user.gender = res.gender;
                    this.user.photo = res.picture.data.url;
                    this.user.facebook = {
                        name: res.name,
                        birthday: res.birthday,
                        gender: res.gender,
                        photo: res.picture.data.url
                    };
                    this.changeUser();
                });
        } else if (provider === 'google') {
            this.GoogleService.getMe()
                .then((user) => {
                    Object.assign(this.user, this.user, user);
                    this.user.google = user;
                    this.changeUser();
                });
        }

    }

    chat(event) {
        let msg = {
            to: this.user.connectionId,
            content: event.currentTarget.value,
            fromName: this.$scope.$parent.$ctrl.users.publishers[0].name,
            fromId: this.$scope.$parent.$ctrl.users.publishers[0].connectionId
        };
        this.SessionService.send('messageTo', msg);
        this.user.chatContent.push(msg);
        this.msg = '';
    }


    updateUser() {
        let msg = {
            method: 'user',
            user: {name: this.user.name, photo: this.user.photo, facebook: this.user.facebook, google: this.user.google},
            from: this.user.connectionId
        };
        this.RoomService.sendRoomMsg(msg);
    }

    showRemoteChat(connId) {
        this.$scope.$parent.$ctrl.showUser(connId);
    }

    removeFile(id) {
        this.user.files.forEach((file, i) => {
            if(file.id == id) {
                this.user.files.splice(i, 1);
            }
        });
    }

    removeFolder(folder) {
        this.user.files.forEach((file, i) => {
            if (file.files) {
                if (file.files === folder) {
                    this.files.splice(i, 1);
                }
            }
        })
    }
}

localUserController.$inject = ['RoomService', '$scope', 'SessionService', 'UserService', 'FacebookService', 'GoogleService'];

export {localUserController};