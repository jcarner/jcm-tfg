import angular from 'angular';
import ngFileSaver from 'angular-file-saver';

import {roomComponent} from './room.component';
import {userComponent} from './user/user.component';
import {localUserComponent} from './localUser/localUser.component';
import {userListComponent} from './userList/userList.component';
import {fileNavComponent} from './fileNav/fileNav.component';
import {roomMenuComponent} from './roomMenu/roomMenu.component';

import {loader} from './loader';
import {notifications} from './notifications';
import {social} from '../../shared/social';
import {player} from './player';

import {RoomService} from './room.service';
import {SessionService} from './session.service';
import {UserService} from './user/user.service';
import {FileService} from './file.service';
import {FileTransferService} from "./fileTransfer.service";

const config = ($stateProvider) => {
    $stateProvider
        .state('app.room', {
            url: '^/room/:name',
            component: 'room',
            resolve: {
                room: ['$stateParams', 'RoomService', 'SessionService', ($stateParams, RoomService, SessionService) => {
                    SessionService.newSession()
                        .then(() => {
                            return {name: $stateParams.name};
                        });
                }]
            }
        });
};

config.$inject = ['$stateProvider'];


export const room = angular.module('room', [
    loader.name,
    notifications.name,
    social.name,
    player.name,

    ngFileSaver
])
    .config(config)
    .component('room', roomComponent)
    .component('localUser', localUserComponent)
    .component('user', userComponent)
    .component('userList', userListComponent)
    .component('fileNav', fileNavComponent)
    .component('roomMenu', roomMenuComponent)
    .factory('RoomService', RoomService)
    .factory('SessionService', SessionService)
    .factory('UserService', UserService)
    .factory('FileService', FileService)
    .factory('FileTransferService', FileTransferService);