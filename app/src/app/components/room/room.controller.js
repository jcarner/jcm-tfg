'use strict';

class roomController {
    constructor($scope, $mdSidenav, $interval,
                FileSaver, RoomService, LoaderService, SessionService, FileService, NotificationService) {
        this.$scope = $scope;
        this.$mdSidenav = $mdSidenav;
        this.$interval = $interval;
        this.RoomService = RoomService;
        this.LoaderService = LoaderService;
        this.SessionService = SessionService;
        this.FileService = FileService;
        this.NotificationService = NotificationService;
        this.FileSaver = FileSaver;

        this.users = {publishers: [], subscribers: []};
        this.roomLink = window.location.href;
        this.files = [];
        this.folders = [];

        this.chat = { active: false, class: 'initial', content: [] };
        this.fileNav = { files: [], folders: []};

        this.usersNav = {
            publ: { offsetX: 50, reduced: true },
            subs: { wrap: false, offsetX: 70, offsetY: 0}
        };

        this.roomMenu = {counter: {chat: 0, files: 0}, menu: {class: ""}, users: 0};
        this.css = { localUser: {} };
        this.class = { filenav: '', localUser: [] };
        this.notifications = NotificationService.notifications;

        $scope.$on('sendMsg', (event, data) => {
            this.session.signal(
                {
                    type: 'chat',
                    data: {from: this.users.publishers[0].name | 'Josep', msg: data.msg}
                }, (error) => {
                    error ?
                        console.log('chat error: ' + error.message) :
                        console.log('chat message sent');

                });
        });

        $scope.$on('setName', this.setName);
        $scope.$on('setUser', this.setUser);
        $scope.$on('message', this.onMessage);
        $scope.$on('messageTo', this.onMessageTo);
        $scope.$on('addPublisher', this.addPublisher);
        $scope.$on('addSubscriber', this.addSubscriber);
        $scope.$on('addLocalUser', this.addLocalUser);
        $scope.$on('addUser', this.addUser);
        $scope.$on('rmUser', this.rmUser);
        $scope.$on('fileData', this.onFileData);
        $scope.$on('fileContent', this.onFileContent);

        document.addEventListener('dragstart', (event) => {
            event.preventDefault();
        }, false);
        document.addEventListener('dragenter', (event) => {
            event.preventDefault();
        }, false);
        document.addEventListener('dragend', (event) => {
            event.preventDefault();
        }, false);
        document.addEventListener('dragleave', (event) => {
            event.preventDefault();
        }, false);
        document.addEventListener('drag', (event) => {
            event.preventDefault();
        }, false);
        document.addEventListener('dragover', (event) => {
            event.preventDefault();
            //this.drag = {file: [{x: event.clientX, y: event.clientY}]};
            this.$scope.$digest();
            //event.dataTransfer.setDragImage(document.getElementById("dragfile"), 0, 0);
        }, false);
        document.addEventListener('drop', (event) => {
            event.preventDefault();
        }, false);

        let subscribers = document.getElementById('subscribers'),
            x = 0, press = false;
        const k = 0.5;
        subscribers.addEventListener('mousemove', (event) => {
            if (press) {
                subscribers.scrollLeft = Math.floor((event.pageX - x) * k)
            }
        }, false);
        subscribers.addEventListener('mousedown', (event) => {
            press = true;
            x = event.pageX;
        }, false);
        subscribers.addEventListener('mouseup', (event) => {
            press = false;
        }, false);

        this.$onInit = () => {
            this.room = { name: window.location.hash.replace('#!/room/','') };
        };

        this.$onDestroy = () => {
            this.SessionService.disconnectSession();
        }
    }


    openMenu($mdOpenMenu, event) {
        $mdOpenMenu(event);
    }

    showUsers() {
        this.$mdSidenav('users').toggle();
    }

    showUser(connId) {
        this.users.subscribers.forEach((user) => {
            if (connId == user.connectionId) {
                user.class = '';
                this.$interval(() => {
                    user.pos = 1;
                    user.class = 'featured';
                }, 500);
            } else {
                user.pos++;
            }
        })
    }

    showChat(event) {
        this.chat.active = true;
        this.chat.class = 'active';
        this.roomMenu.counter.chat = 0;
        this.roomMenu.class = "";
        this.NotificationService.notifications.chatNotification = [];
    }

    sendFile(file) {
        this.RoomService.sendFile(file);
    }

    addUser(event, data) {
        let self = event.currentScope.$ctrl;
        let user = self.findUser(data.connectionId);
        if (user === null) {
            self.users.subscribers.push({
                streamId: `subscriber-${self.users.subscribers.length + 1}`,
                connectionId: data.connectionId,
                chatContent: []
            });
        }
        self.roomMenu.users++;
        self.$scope.$digest();
    }

    addLocalUser(event, data) {
        let self = event.currentScope.$ctrl;
        self.users.publishers.push({
            streamId: 1, connectionId: data.id,
            name: '',
            files: [], folders: [],
            chats: [],
            notify: {files: 0, menu: ''}
        });
        self.$scope.$digest();
    }

    updateUser(changes) {
        switch (changes.target) {
            case 'chat':
                let chat = this.findChat(changes.id);
                chat.msgCounter = 0;
                break;
            default:
                break;
        }
    }

    addPublisher(event, data) {
        let self = event.currentScope.$ctrl;
        if (self.users.publishers[0].publisher) {
            Object.assign(self.users.publishers[0], self.users.publishers[0], {
                connectionId: data.id,
                name: '',
                on: {video: true, audio: true}
            });
        } else {
            self.users.publishers[0].connectionId = data.id;
        }
        self.$scope.$digest();
    }

    addSubscriber(event, data) {
        let self = event.currentScope.$ctrl;
        let user = self.findUser(data.id);
        user.stream = data.stream;
        user.streamId = 'stream-' + data.id;
        self.$scope.$digest();
    }

    rmUser(event, data) {
        let self = event.currentScope.$ctrl;
        for (let i = 0; i < self.users.subscribers.length; i++) {
            if (self.users.subscribers[i].connectionId == data.connectionId) {
                self.users.subscribers.splice(i, 1);
            }
        }
        self.roomMenu.users--;
        self.$scope.$digest();
    }

    setName(event, data) {
        let self = event.currentScope.$ctrl,
            user = self.findUser(data.from);
        user.name = data.name;
        self.$scope.$digest();
    }

    setUser(event, data) {
        let self = event.currentScope.$ctrl,
            user = self.findUser(data.from);
        Object.assign(user, user, data.user);
        self.$scope.$digest();
    }

    findUser(connectionId) {
        for (let user of this.users.subscribers) {
            if (user.connectionId == connectionId) {
                return user;
            }
        }
        for (let user of this.users.publishers) {
            if (user.connectionId == connectionId) {
                return user;
            }
        }
        return null;
    }

    findFile(fileId, folderId) {
        let result = null;
        if (!folderId) {
            this.fileNav.files.forEach((file) => {
                if (fileId == file.id) {
                    result = file;
                }
            });
            if (result == null) {
                this.fileNav.files.push({id: fileId, chunks: []});
                return this.findFile(fileId, null);
            }
        } else {
            let folder = this.findFolder(folderId);
            folder.files.forEach((file) => {
                if (fileId == file.id) {
                    result = file;
                }
            });
            if (result == null) {
                folder.files.push({id: fileId, folderId: folderId, chunks: []});
                return this.findFile(fileId, folderId);
            }
        }
        return result;
    }

    findPersonalFile(fileId, folderId) {
        let result = null;
        let user = this.users.publishers[0];

        if (!folderId) {
            user.files.forEach((file) => {
                if (fileId == file.id) {
                    result = file;
                }
            });
            if (result == null) {
                user.files.push({id: fileId, chunks: []});
                return this.findPersonalFile(fileId, null);
            }
        } else {
            let folder = this.findPersonalFolder(folderId);
            folder.files.forEach((file) => {
                if (fileId == file.id) {
                    result = file;
                }
            });
            if (result == null) {
                folder.files.push({id: fileId, chunks: []});
                return this.findPersonalFile(fileId, folderId);
            }
        }
        return result;
    }

    findPersonalFolder(folderId) {
        let result = null;
        let user = this.users.publishers[0];
        user.folders.forEach((folder) => {
            if (folder.id == folderId) {
                result = folder;
            }
        });
        if (result == null) {
            user.folders.push({id: folderId, files: []});
            return this.findPersonalFolder(folderId);
        }
        return result;
    }

    findFolder(id) {
        let result = null;
        this.fileNav.folders.forEach((folder) => {
            if (folder.id == id) {
                result = folder;
            }
        });
        if (result == null) {
            this.fileNav.folders.push({id: id, files: []});
            return this.findFolder(id);
        }
        return result;
    }


    findChat(connectionId) {
        let result = null;
        this.users.publishers[0].chats.forEach((chat) => {
            if (chat.with.connectionId == connectionId) {
                result = chat;
            }
        });
        if (result == null) {
            let user = this.findUser(connectionId);
            this.users.publishers[0].chats.push({
                    with: {
                        name: user.name ? user.name : user.streamId,
                        img: user.photo,
                        connectionId: connectionId
                    },
                    msgCounter: 0
                }
            );
            result = this.findChat(connectionId);
        }
        return result;
    }

    onFileData(event, data) {
        let self = event.currentScope.$ctrl;
        let file = null;
        if (data.to == 'global') {
            self.newFile();
            if (data.folderId) {
                file = self.findFile(data.id, data.folderId);
            } else {
                file = self.findFile(data.id);
            }
        } else {
            let user = self.findUser(data.to);
            let from = self.findUser(data.from);
            self.newPersonalFile();
            Object.assign(data, data, {from: from.name == '' ? from.streamId : from.name});
            file = self.findPersonalFile(data.id, data.folderId || null);
        }
        Object.assign(file, file, data);
        self.$scope.$digest();
    }

    onFileContent(event, data) {
        let self = event.currentScope.$ctrl;
        let file = null;

        if (data.to == 'global') {
            file = data.folderId ? self.findFile(data.id, data.folderId) : self.findFile(data.id, null);
        } else {
            file = data.folderId ? self.findPersonalFile(data.id, data.folderId) : self.findPersonalFile(data.id, null);
        }

        if (data.blob) {
            file.blob = data.blob;
            self.FileSaver.saveAs(file.blob, file.name);
            self.recvChannel.close();
        } else if (data.chunk) {
            file.chunks.push(data.chunk);
            file.progress += data.chunk.length / file.size * 100;
            // console.log('Chunk received:' + data.chunk.length + 'Progress:' + file.progress);
            if (data.last) {
                file.blob = self.FileService.dataURItoBlob(file.chunks.join(''));
                file.class = 'complete';
                console.log(`File(${file.id}) received`);
            }
        }
        self.$scope.$digest();
    }


    newFile() {
        this.roomMenu.counter.files++;
        this.roomMenu.class = "";
        this.$scope.$digest();
        this.roomMenu.class = "alerted";
        this.$scope.$digest();
        if (this.roomMenu.counter.chat > 3) {
            this.roomMenu.class = "md-warn";
        }
    }

    newPersonalFile() {
        let user = this.users.publishers[0];
        user.notify.files++;
        user.notify.menu = 'alerted';
        this.$scope.$digest();
        user.notify.menu = 'alerted';
        this.$scope.$digest();
        user.notify.files > 3 ? user.notify.menu = 'alerted fixed' : null;
    }

    onMessage(event, data) {
        let self = event.currentScope.$ctrl;
        try {
            let from = self.findUser(data.from);
            data.fromName = from.name;
        } catch(err) { data.fromName = 'Unknown' }

        data.type = 'external';
        self.chat.content.push(data);
        self.newMessage();
        self.$scope.$digest();
    }

    newMessage() {
        this.roomMenu.counter.chat++;
        this.roomMenu.class = 'alerted';
        this.$scope.$digest();
    }

    onMessageTo(event, data) {
        let self = event.currentScope.$ctrl,
            me = self.users.publishers[0],
            from = self.findUser(data.from),
            chat = self.findChat(data.from);
        if (from != null) {
            self.newPrivateChat(data.from);
            data.fromName = from.name ? from.name : from.streamId;
            data.type = 'external';
            from.chatContent.push(data);
            chat.msgCounter++;
        }
        self.$scope.$digest();
    }

    newPrivateChat(fromId) {
        let me = this.users.publishers[0];
        let from = this.findUser(fromId);
        Object.assign(me, me, {notify: {menu: ''}});
        Object.assign(from, from, {notify: {menu: ''}});
        this.$scope.$digest();
        me.notify.menu = 'alerted';
        from.notify.menu = 'alerted';
        this.$scope.$digest();
    }

    updatePlayer(player) {
        this.player = player;

        if (player.width) {
            this.css.localUser.maxWidth =  90 - player.width;
            if(!this.usersNav.publ.reduced || player.width == 55) {
                this.usersNav.publ.offsetX = player.width;
                // this.class.localUser.find((c) => {return c == 'start'}) ?
                //     null : this.class.localUser.push('start');
            } else {
                this.class.localUser = [];
                this.usersNav.publ.offsetX = 0;
            }
        }

        if(player.height) {
            if (player.height > 55) {
                this.usersNav.subs = {offsetX: 0, offsetY: Math.max(0, player.height - 55)+'%', wrap: false};
                if(player.height > 70) {
                    this.usersNav.subs = {wrap: true, offsetX: player.width, offsetY: '0%'};
                }
            } else {
                this.usersNav.subs = {wrap: false, offsetX: 0, offsetY: Math.max(0, player.height - 55)+'%'};
            }
        }
    }

    updateLocalUser(localUser) {
        if (localUser.$reduce) {
            this.usersNav.publ = { offsetX: this.player.width, reduced: true };
            // if (this.player.width == 33) {
            //     this.class.localUser.forEach((c, i) => {
            //         if (c == 'start') { this.class.localUser.splice(i, 1); }
            //     });
            // } else {
            //     this.class.localUser.find((c) => { return c == 'start'}) ?
            //         null : this.class.localUser.push('start');
            // }
            this.css.localUser.width = '';
        }
        if (localUser.$reduce == false) {
            // if (!this.class.localUser.find((c) => {return c === 'start'})) {
            //     this.class.localUser.push('start');
            // }
            this.css.localUser.width = 95 - this.player.width;
            this.usersNav.publ = { offsetX: this.player.width, reduced: false };
        }
    }

    updateMenu(menu) {
        switch(menu.show) {
            case 'users':
                this.$mdSidenav("users").toggle();
                break;
            case 'chat':
                this.showChat();
                break;
        }
    }

    toggleSubscriberView() {
        this.usersNav.subs.wrap = !this.usersNav.subs.wrap;
    }
}

roomController.$inject = [
    '$scope',
    '$mdSidenav',
    '$interval',
    'FileSaver',
    'RoomService',
    'LoaderService',
    'SessionService',
    'FileService',
    'NotificationService'
];

export {roomController};