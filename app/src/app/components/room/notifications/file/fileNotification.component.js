import template from './fileNotification.html';
import './fileNotification.scss';
import {fileNotificationController} from './fileNotification.controller';

export const fileNotificationComponent = {
    template,
    controller: fileNotificationController,
    bindings: {

    }
};