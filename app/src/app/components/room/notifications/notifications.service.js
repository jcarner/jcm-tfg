
const NotificationService = ($q,$interval) => {

    let notifications = {
        fileNotifications: [],
        chatNotifications: [],
        fileToNotification: [],
        chatToNotification: []
    };

    const showFileNotification = (file, from) => {
        let notification = {};
        notification.file = file;
        notification.from = from;
        notification.accept = null;
        notifications.fileNotifications.push(notification);

        let defer = $q.defer();
        setTimeout(()=>{
            $interval.cancel(interval);
            defer.reject();
        },10000);
        let interval = $interval(()=>{
            if (notifications.fileNotifications[0].accept !== null) {
                if (notifications.fileNotifications[0].accept) {
                    notifications.fileNotifications.splice(0,1);
                    $interval.cancel(interval);
                    defer.resolve();
                } else {
                    notifications.fileNotifications.splice(0,1);
                    $interval.cancel(interval);
                    defer.reject();
                }
            }

        },1000);
        return defer.promise;
    };
    const closeFileNotification = (accept) => {
        notifications.fileNotifications[0].accept = accept;
    };

    const newMessageNotification = () => {

    };

    const addNotification = (notification) => {
        notifications[notification.type].push(notification);
    };

    const resetNotification = (notification) => {
        notifications[notification] = [];
    };

    return {
        notifications,
        showFileNotification,
        closeFileNotification,

        addNotification,
        resetNotification
    }
};

NotificationService.$inject = ['$q','$interval'];

export {NotificationService};