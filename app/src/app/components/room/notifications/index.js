import angular from 'angular';

import {fileNotificationComponent} from './file/fileNotification.component';

import {NotificationService} from './notifications.service';

const config = () => {

};

config.$inject = [];

export const notifications = angular.module('app.room.notifications', [])
    .config(config)
    .component('fileNotification', fileNotificationComponent)
    .factory('NotificationService', NotificationService);