import template from './roomMenu.html';
import './roomMenu.scss';
import {roomMenuController} from "./roomMenu.controller";

export const roomMenuComponent = {
    template,
    controller: roomMenuController,
    bindings: {
        data: '<',
        onUpdate: '&'
    }
};