class roomMenuController {
    constructor($scope) {
        this.$scope = $scope;


        this.class = {};

        this.$scope.$on('addTransfer', this.onAddTransfer);
        this.$scope.$on('transferProgressUpdate', this.onTransferUpdate);

        this.$onInit = () => {
            this.roomLink = window.location.href;
            this.data.transfers = [];
        };
    }

    open($mdOpenMenu, event) {
        $mdOpenMenu(event);
    }

    openUserList() {
        this.onUpdate({ menu: {show: 'users'} });
    }

    openChat() {
        this.onUpdate({ menu: {show: 'chat'} });
    }

    findTransfer(id) {
        let res = null;
        this.data.transfers.forEach((transfer) => {
            if(transfer.id == id) { res = transfer }
        });
        if(res == null) {
            this.data.transfers.push({id: id, progress: 0});
            res = this.findTransfer(id);
        }
        return res;
    }

    onAddTransfer(event, data) {
        let self = event.currentScope.$ctrl;
        let transfer = self.findTransfer(data.id);
        Object.assign(transfer, transfer, data);
    }

    onTransferUpdate(event, data) {
        let self = event.currentScope.$ctrl;
        let transfer = self.findTransfer(data.id);
        if(data.total) { transfer.total = data.total; }
        transfer.progress += (data.progress / transfer.total) * 100;
        if(data.last) { transfer.progress = 100 }
    }

    copyLink() {
        copy(this.roomLink);
    }

    test() {
        debugger;
    }
}

roomMenuController.$inject = ['$scope'];

export {roomMenuController};