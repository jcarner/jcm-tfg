const LoaderService = ($rootScope, $window) => {

    const errors = {
        TOKEN_EXPIRED: 0
    };
    const messages = {error: {}};
    messages.error[errors.TOKEN_EXPIRED] = '';

    const start = () => {
        $rootScope.$broadcast('loaderOn');
    };

    const stop = () => {
        $rootScope.$broadcast('loaderOff');
    };

    const reconnect = () => {
        $rootScope.$broadcast('roomReconnect');
    };

    const error = () => {
        $rootScope.$broadcast('loaderError');
    };

    const connected = () => {
        $rootScope.$broadcast('roomConnected');
    };

    return {
        start,
        stop,
        reconnect,
        error,
        connected,

    }
};

LoaderService.$inject = ['$rootScope', '$window'];

export {LoaderService};