import angular from 'angular';

import {loaderComponent} from './loader.component';

import {LoaderService} from './loader.service';

export const loader = angular.module('loader', [])
    .component('loader', loaderComponent)
    .factory('LoaderService', LoaderService);