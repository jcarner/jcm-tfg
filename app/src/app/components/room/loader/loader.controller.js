class loaderController {
    constructor($scope, LoaderService, SessionService) {
        let self = this;
        this.SessionService = SessionService;
        this.message = 'Connecting with the room';
        this.isVisible = true;
        this.show = {error: false};

        $scope.$on('loaderOn', () => {
            this.connect();
            $scope.$digest();
        });
        $scope.$on('loaderOff', () => {
            this.turnOff();
            $scope.$digest();
        });
        $scope.$on('roomReconnect', () => {
            this.reconnecting();
            $scope.$digest();
        });
        $scope.$on('loaderError', (event) => {
            this.error();
            $scope.$digest();
        });
        $scope.$on('roomConnected', () => {
            this.message = 'Connected';
            $scope.$digest();
        });
    }

    change() {
        this.isVisible = !this.isVisible;
    }

    connect() {
        this.isVisible = true;
        this.show.error = false;
        this.message = 'Connecting';
    }

    reconnecting() {
        this.isVisible = true;
        this.show.error = false;
        this.message = 'Reconnecting';
    }

    reconnect() {
        // this.SessionService.disconnectSession();
        // this.SessionService.connectSession();
        this.SessionService.reconnectSession();
    }

    turnOff() {
        this.isVisible = false;
        this.message = '';
    }

    error() {
        this.isVisible = true;
        this.show.error = true;
        this.message = 'Connection error';
    }

}

loaderController.$inject = ['$scope', 'LoaderService', 'SessionService'];

export {loaderController};