import template from './loader.html';
import './loader.scss';
import {loaderController} from './loader.controller';

export const loaderComponent = {
    template,
    controller: loaderController
};