'use strict';
class Session {
    constructor(server) {
        this.server = server;
        this.connections = [];
    }

    addConnections(list) {
        this.connections = list;
    }

    addConnection(conn) {
        this.connections.push(conn);
    }

    rmConnection(id) {
        this.connections.forEach((conn, i) => {
            if(conn == id)  this.connections.splice(i, 1);
        });
    }

    send(msg) {
        this.server.send(JSON.stringify(msg));
    }
}

const SessionService = ($window, RoomService, LoaderService, PlayerService, $http, $q) => {

    let apiKey = '', sessionId = '', token = '';

    let connectionId = '';

    let session = null;

    let wsServer = null;

    const newSession = (url) => {
        if(!url) { url = window.location.hash }
        return $http({
            method: 'GET',
            url: `https://${__API_URL__}:3000/session`,
            params: {url: url}
        })
            .then((res) => {
                session = initSession(res.data.session);
                return session;
            })
            .catch((err) => {
                console.log('Call HTTPS to /session failed');
                return $http({
                    method: 'GET',
                    url: `http://${__API_URL__}:3001/session`,
                    params: {url: url}
                })
                .then((res) => {
                    session = initSession(res.data.session);
                    return session;
                })
                .catch((err) => {
                    console.log('Call HTTP to /session failed');
                    session = initSession(null);
                });
            });
    };

    const initSession = (sessionId) => {
        wsServer = new WebSocket(`wss://${__API_URL__}:5000`);
        wsServer.onopen = () => {
            RoomService.setSession(session);
            LoaderService.connected();
            console.log('WEBSOCKET CHANNEL OPENED');
            wsServer.send(JSON.stringify({method: 'session', session: sessionId, url: window.location.hash}));
        };
        wsServer.onmessage = (res) => {
            let msg = JSON.parse(res.data);
            switch(true) {
                case 'id' in msg:
                    RoomService.addLocalUser(msg.id);
                    LoaderService.stop();
                    break;
                case 'delete' in msg:
                    RoomService.rmUser(msg.delete);
                    break;
                case 'add' in msg:
                    RoomService.addUser(msg.add);
                    break;
                case 'list' in msg:
                    session.addConnections(msg.list);
                    msg.list.forEach((id) => {
                        RoomService.addUser(id);
                        RoomService.createPeerConnection(id);
                        RoomService.createDataChannels(id);
                    });
                    break;
            }

            switch(msg.method) {
                case 'offer':
                    RoomService.onOffer(msg);
                    break;
                case 'answer':
                    RoomService.onAnswer(msg);
                    break;
                case 'candidate':
                    RoomService.onCandidate(msg);
                    break;
            }

        };
        wsServer.onclose = (err) => {
            console.log('WebSocket Closed: ' + err);
            LoaderService.error();
        };

        return new Session(wsServer);
    };

    const getSession = () => {
        let deferred = $q.defer();
        if (session === null) {
            newSession()
                .then((res) => {
                    session = res;
                    deferred.resolve(session);
                });
        } else {
            deferred.resolve(session);
        }
        return deferred.promise;
    };


    const disconnectSession = () => {
        session.server.onclose = () => { console.log('Session disconnected'); };
        session.server.close();
    };

    const reconnectSession = () => {
        disconnectSession();
        RoomService.restartRoom();
        newSession();
    };

    const send = (type, msg) => {
        wsServer.send(JSON.stringify({method: type, msg: msg}));
    };

    return {
        newSession,
        initSession,
        disconnectSession,
        reconnectSession,
        getSession,
        send
    }
};

SessionService.$inject = ['$window', 'RoomService', 'LoaderService', 'PlayerService', '$http', '$q'];

export {SessionService};
