
import angular from 'angular';

import {homeComponent} from './home.component';


const config = ($stateProvider) => {
    'use strict';
    $stateProvider
        .state('app.home', {
            url: '^/',
            component: 'home'
        });
};

config.$inject = ['$stateProvider'];

const home = angular.module('home', [])
    .config(config)
    .component('home', homeComponent);

export {home};