class homeController {
    constructor($state) {
        this.$state = $state;

        this.room = {name: ''};
        this.base = document.location.href;
        this.link = {link: true};
    }

    enterRoom() {
        this.$state.go('app.room',{name: this.room.name});
    }
}

homeController.$inject = ['$state'];

export {homeController};