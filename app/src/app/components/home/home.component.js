
import {homeController} from './home.controller';
import template from './home.html';
import './home.scss';

export const homeComponent = {
    controller: homeController,
    template
};
