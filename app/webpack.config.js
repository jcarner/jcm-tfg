var webpack = require('webpack');
var path = require('path');
var pkg = require('./package.json');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var modRewrite = require('connect-modrewrite');

// Path Config
var pathConfig = {
    context: path.join(__dirname, 'src'),
    distPath: path.join(__dirname, 'www'),
    assetsPath: path.join(__dirname, 'www/assets')
};

const API_URL = process.env.API_URL || 'localhost';
const SSL_CERT = process.env.SSL_CERT || '/home/d7th/jcm-tfg/server/cert.pem';
const SSL_KEY = process.env.SSL_KEY || '/home/d7th/jcm-tfg/server/key.pem';

// varPlugin takes raw strings and inserts them, so you can put strings of JS if you want.
var varPlugin = new webpack.DefinePlugin({
    __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
    __API_URL__: JSON.stringify(process.env.API_URL || 'localhost'),
    __VERSION__: JSON.stringify(require('./package.json').version),
    __FB_APP_ID__: JSON.stringify(process.env.FB_API_KEY || '326890037706883'),
    __GAPP_ID__: JSON.stringify(process.env.GAPP_ID || '1032972873840-d44m3d5g22ajhu9ogremaltbnkpb9voe.apps.googleusercontent.com'),
    __GAPI_KEY__: JSON.stringify(process.env.GAPI_KEY || 'AIzaSyCyaMMry5nqXlOorYYZGOg6CONTNwpLt74'),
    __TURN_USER__: JSON.stringify(process.env.TURN_USER || 'turnuser'),
    __TURN_PASS__: JSON.stringify(process.env.TURN_PASS || 'turnpass')
});

var config = {
    context: pathConfig.context,
    entry: './index.js',
    debug: true,
    devtool: 'source-map',
    output: {
        publicPath: '/',
        path: pathConfig.context,
        filename: 'bundle-[hash:6].js'
    },

    plugins: [
        new ExtractTextPlugin('styles-[hash:6].css'),
        varPlugin,
        new HtmlWebpackPlugin({
            filename: 'index.html',
            pkg: pkg,
            template: 'index.html'
        }),
        new BrowserSyncPlugin(
            {
		port: 7000,
                proxy: {target: 'http://localhost:6999', ws: true},
		open: false,
		ghostMode: false,
		https: {
		   key: SSL_KEY,
         	   cert: SSL_CERT
		},
                middleware: [
                    modRewrite([
                        '!\\.\\w+$ /index.html [L]'
                    ])
                ],
                ui: { port: 7001 }
            },
            {
                reload: false
            }
        )
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'ng-annotate!babel',
                exclude: /node_modules/
            },
            {
                test: /\.(json|svg)$/,
                loader: 'raw',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: 'raw!html-minifier',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader?sourceMap', 'css?sourceMap')
            },
            {
                test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader?name=[name].[ext]?[hash]'
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'url-loader?limit=8192'
            },
            {
                test: /\.scss$/,
                loader: 'style!css!sass'
            },
            {
                test: /\.json$/, loader: "json-loader"
            },
            {
                test: /\.(svg)$/, loader: "svg-url"
            }
        ]
    },
    sassLoader: {
        includePaths: [path.resolve(__dirname, './src')]
    },
    'html-minifier-loader': {
        removeComments: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        preserveLineBreaks: false
    }
};

if (process.env.NODE_ENV === 'production') {
    config.devtool = null;
    config.output.path = pathConfig.distPath;
    config.output.publicPath = '/';
    config.plugins = [
        varPlugin,
        new webpack.optimize.DedupePlugin(),
        new ExtractTextPlugin('styles-[hash:6].css'),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            pkg: pkg,
            template: 'index.html',
            minify: {
                collapseInlineTagWhitespace: true,
                conservativeCollapse: true,
                removeComments: true
            }
        })
    ];
}

module.exports = config;
