# README #

Josep Carner Madrigal's Bachelor's Degree Final Project public repository.

## Development of a web application about multimedia rooms using WebRTC technology ##

The application is allowing create rooms accessible under a unique URL. The users coexisting in the same room can use the multimedia services in that context. It hasn't been developed any user account system. The usage of the room's services is anonymous, but the user can fill its profile or load it from Facebook's or Google's accounts.

Main technologies

* AngularJS as front-end framework
* NodeJS as back-end framework
* WebRTC for real-time multimedia purposes

### Multimedia Services ###

* Chat
* Video-chat
* File-transfer
* Music/Video player


