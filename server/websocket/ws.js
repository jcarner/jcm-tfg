let ws = require('nodejs-websocket');
let dao = require('../mysql/DAO.js');
let SessionService = require('../session');
let fs = require('fs');

class wsRoomServer {
    constructor(roomURL) {
	let options = {secure: true, key: fs.readFileSync('key.pem'), cert: fs.readFileSync('cert.pem')};
        let server = ws.createServer(options, (conn) => {
            let session = null;
            //create a connection id
            conn.id = Math.random().toString(36).substring(7);
            conn
                .on('text', (text) => {
                    let msg = JSON.parse(text);
                    switch (msg.method) {
                        case 'session':
                            //retrieve the session and add the connection
                            if(!msg.session) {
                                session =
                                    SessionService.SessionService().getSession(msg.url)
                                        .then((res) => {

                                        });
                            } else {
                                session = SessionService.SessionService().getSessionByID(msg.session);
                            }

                            try {
                                conn.sendText(JSON.stringify({id: conn.id}));
                                session.addConn(conn);
                            } catch(err) {
                                console.log('Cannot add connection:'+ err)
                            }
                            break;
                        case 'user':
                            msg.from = conn.id;
                            session.broadcast(msg);
                            break;
                        case 'chat':
                            break;
                        case 'offer':
                        case 'answer':
                        case 'candidate':
                            msg.from = conn.id;
                            session.sendMsg(msg, msg.to);
                            break;
                    }
                })
                .on('error', (err) => {
                    if(err.code !== 'ECONNRESET')
                        console.log('Websocket Error:\t' + err);
                })
                .on('close', (code, reason) => {
                    try {
                        console.log('Websocket Connection closed');
                        session.rmConn(conn.id);
                    } catch(err) {
                        console.log(err);
                    }

                });
        }).listen(5000);
        this.server = server;
        this.roomURL = roomURL;
    }

}

module.exports = wsRoomServer;
