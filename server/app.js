const express = require('express');
const app = express();
const path = require('path');
const https = require('https'),
    fs = require('fs');
const ws = require('./websocket/ws');

let dao = require('./mysql/DAO.js');
let ws1 = new ws('localhost');

let SessionService = require('./session');

//endpoints
app.get('/session', (req, res) => {
    res.header('Access-Control-Allow-Origin', '*');
    SessionService.SessionService().getSession(req.param('url'))
        .then((r) => {
            //return sessionID
            res.json({session: r.session.id});
        });
});

app.get('/testWS', (req, res) => { res.sendFile(path.join(__dirname + '/websocket/test.html')) });

//start HTTP/HTTPS server
app.listen(3001, () => { console.log('HTTP Server STARTED') });
https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
}, app).listen(3000, () => { console.log('HTTPS Server STARTED') });