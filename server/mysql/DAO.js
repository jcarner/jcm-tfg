let mysql = require('mysql');
const promise = require('promised-io/promise');

class DAO {
    constructor() {
        this.connect();
    }

    connect() {
        let config = {
            host: process.env.DB_HOST || 'localhost',
            user: process.env.DB_USER || 'mysql',
            password: process.env.DB_PASS || 'pracPrac&!',
            database: process.env.DB_NAME || 'tfg'
        };

        this.conn = mysql.createConnection(config);
        this.conn.on('error', (err) => {
            console.log('Server disconnected due to ' + err);
            this.connect();
        });
        this.conn.connect();
    }

    getSessionByURL(URL) {
        let defer = new promise.Deferred();
        this.conn.query(
            "SELECT * FROM session_url WHERE `url` = ?", [URL],
            (err, rows, fields) => {
                if (!err && rows.length > 0)
                    defer.resolve({sessionId: rows[0].session_id});
                else
                    defer.reject();
            }
        );
        return defer.promise;
    }

    createSession(sessionId, URL) {
        let defer = new promise.Deferred();
        this.conn.query(
            'INSERT INTO session_url SET ?', {url: URL, session_id: sessionId},
            (err, res, fields) => {
                if(err) {
                    console.log(JSON.stringify(err));
                    this.conn.query('UPDATE session_url SET ? WHERE ?', [{session_id: sessionId}, {url: URL}],
                        (err) => {
                            if(!err) {
                                defer.resolve();
                                console.log('Database: Session ID UPDATED');
                            } else {
                                defer.reject();
                                console.log(JSON.strigify(err));
                            }

                        });
                } else {
                    defer.resolve();
                    console.log('Database: new session ADDED');
                }
            }
        );
        return defer.promise;
    }

    removeSession(sessionId) {
        let defer = new promise.Deferred();
        this.conn.query(
            `DELETE FROM session_url WHERE session_id = '${sessionId}'`,
            (err, res) => {
                if(err) {
                    defer.reject();
                    console.log(JSON.strigify(err));
                } else {
                    defer.resolve();
                    console.log('SESSION_URL deletes ' + res.affectedRows + ' rows');
                }
            }
        );
        return defer.promise;
    }
}

let dao = new DAO();

module.exports = dao;