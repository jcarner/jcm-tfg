let dao = require('./mysql/DAO');
let promise = require('promised-io/promise');

class Session {
    constructor(id) {
        this.conn = [];
        this.id = id;
    }

    addConn(conn) {
        let list = [];
        this.conn.forEach((conn) => {
            list.push(conn.id);
        });
        conn.sendText(JSON.stringify({list: list}));
        this.conn.push(conn);
        this.broadcast({ msg:  {add: conn.id}, from: conn.id });
    }

    rmConn(id) {
        this.conn.forEach((conn, i) => {
            if(conn.id == id) this.conn.splice(i, 1);
        });
        this.broadcast({ msg: {delete: id}, from: '' });
        if(this.conn.length == 0) {
            SessionService().removeSession(this.id);
        }
    }

    sendMsg(msg, to) {
        this.conn.forEach((conn) => {
            if (to == conn.id) {
                try {
                    conn.sendText(JSON.stringify(msg));
                } catch(err) { console.log('send msg fails: '+ err) }
            }
        });
    }

    broadcast(msg) {
        this.conn.forEach((conn) => {
            if (msg.from != conn.id) {
                try {
                    conn.sendText(JSON.stringify(msg.msg));
                } catch(err) { console.log('broadcast msg fails: '+ err) }
            }
        });
    }

    updateUser(user) {
        user.from = '';
        this.broadcast(user);
    }
}


let sessions = [];

let SessionService = () => {

    const createSession = (url) => {
        let defer = new promise.Deferred();
        //generate session ID
        let sessionID = Math.random().toString(36).substring(7);
        //save in database
        dao.createSession(sessionID, url).then(
            (res) => {
                //create new session
                let session = new Session(sessionID);
                sessions.push(session);
                defer.resolve({session: session});
            }, () => {
                defer.reject();
                console.log('DAO.createSession error');
            });

        return defer.promise;
    };

    const getSession = (url) => {
        let defer = new promise.Deferred();
        //check if session url exists in database
        dao.getSessionByURL(url).then(
            (res) => {
                session = getSessionByID(res.sessionId);
                if(session == null) {
                    session = new Session(res.sessionId);
                    sessions.push(session);
                }
                defer.resolve({session: session});
            }, () => {
                //create new session
                createSession(url).then(
                    (res) => {
                        defer.resolve({session: res.session});
                    }, () => {
                        defer.reject();
                        console.log('SessionService.createSession error');
                    });
            });

        return defer.promise;
    };

    const getSessionByID = (id) => {
        let result = null;
        sessions.forEach((session) => {
            if(session.id == id) {
                result = session;
                return;
            }
        });

        return result;
    };

    const removeSession = (id) => {
        console.log('Removing session ' + id);
        dao.removeSession(id);
        sessions.forEach((session, i) => {
            if(session.id == id) sessions.splice(i, 1);
        });
    };

    return {
        createSession,
        removeSession,
        getSession,
        getSessionByID
    }
};

module.exports = {SessionService, Session};